import numpy as np
import fireworks.ic as fic
import time
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn

N = np.array([100, 500, 1000, 1500, 2000, 2500])
mass_min = 1
pos_min = 1
vel_min = 1
mass_max = 100
pos_max = 100
vel_max = 100

t_dir = np.zeros(len(N))
t_vec = np.zeros(len(N))
t_py = np.zeros(len(N))

for i in range (len(N)):

    particle = fic.ic_random_uniform(N[i], mass_min, mass_max, pos_min, pos_max, vel_min, vel_max)
    
    t_dir1 = time.perf_counter()
    acc_dir, jerk_dir,_ = fdyn.acceleration_direct(particle)
    t_dir2 = time.perf_counter()
    dt_dir = t_dir2 - t_dir1
    t_dir[i] = dt_dir

    t_vec1 = time.perf_counter()
    acc_vec, jerk_vec,_ = fdyn.acceleration_direct_vectorised(particle)
    t_vec2 = time.perf_counter() 
    dt_vec = t_vec2 - t_vec1 
    t_vec[i] = dt_vec 

plt.plot(t_dir, N, color='r', marker='o', label = 'acceleration_direct')
plt.plot(t_vec, N, color='b', marker='o', label = 'acceleration_direct_vectorised')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('log(t)')
plt.ylabel('log(N)')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_jerk.png')
plt.show()