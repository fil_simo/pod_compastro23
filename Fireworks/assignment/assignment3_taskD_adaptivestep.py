import numpy as np
import fireworks.ic as fic
import time
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
import fireworks.nbodylib.timesteps as fstep
from fireworks.particles import Particles

mass_1 = 1
mass_2 = 10
ecc = 0.99
r_p = 1.0

particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
integrator = Particles.Etot
Etot, Ekin, Epot =integrator (particles, softening = 0.)

a = r_p/(1.-ecc)
Mtot = mass_1 + mass_2
P = np.sqrt((4*np.pi**2*a**3)/(Mtot))

pos_E = [particles.pos, ]
vel_E = [particles.vel, ]
Etot_E = [Etot,]
deltaE_E=[0.,]
pos_LF = [particles.pos, ]
vel_LF = [particles.vel, ]
Etot_LF = [Etot,]
deltaE_LF=[0.,]
pos_RK = [particles.pos, ]
vel_RK = [particles.vel, ]
Etot_RK = [Etot,]
deltaE_RK=[0.,]
pos_T = [particles.pos, ]
vel_T = [particles.vel, ]
Etot_T = [Etot,]
deltaE_T=[0.,]
t = 0.
h = 0.01
tstep_E = [t, ]
tstep_LF = [t, ]
tstep_RK = [t, ]
tstep_T = [t, ]
acc = 0.
ts, jerk, pot= 0., 0., 0.
Etot, Ekin, Epot = 0., 0., 0.

t0_E = time.time()
while (t < 3*P):
    particles, ts , acc, jerk , pot = fint.integrator_Euler(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_E.append(particles.pos.copy())
    vel_E.append(particles.vel.copy())
    h=fstep.adaptive_timestep(particles, None, None)
    t+=h
    tstep_E.append(t)
    Etot, Ekin, Epot = integrator(particles, softening = 0.)
    deltaE= (Etot-Etot_E[-1])/Etot 
    Etot_E.append (Etot)
    deltaE_E.append(deltaE)
t1_E = time.time()

pos_E=np.array(pos_E)

X_cdm = (mass_1*pos_E[:,0,0] + mass_2*pos_E[:,1,0])/(mass_1 + mass_2)
Y_cdm = (mass_1*pos_E[:,0,1] + mass_2*pos_E[:,1,1])/(mass_1 + mass_2)

X1_E = pos_E[:,0,0]
Y1_E = pos_E[:,0,1]

X2_E = pos_E[:,1,0]
Y2_E = pos_E[:,1,1]

plt.plot(X1_E-X_cdm, Y1_E-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X2_E-X_cdm, Y2_E-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Adaptive timestep Euler')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Eulero_ecc_adaptivetimestep.png')
plt.show()

particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
t = 0.
acc = 0.
h = 0.01
ts, jerk, pot= 0., 0., 0.

t0_LF = time.time()
while (t < 3*P):
    particles, ts , acc, jerk , pot = fint.integrator_Leapfrog(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_LF.append(particles.pos.copy())
    vel_LF.append(particles.vel.copy())
    h=fstep.adaptive_timestep(particles, None, None)
    t+=h
    tstep_LF.append(t)
    Etot, Ekin, Epot = integrator(particles, softening = 0.)
    deltaE= (Etot-Etot_LF[-1])/Etot 
    Etot_LF.append (Etot)
    deltaE_LF.append(deltaE)
t1_LF = time.time()

pos_LF = np.array(pos_LF)

X_cdm = (mass_1*pos_LF[:,0,0] + mass_2*pos_LF[:,1,0])/(mass_1 + mass_2)
Y_cdm = (mass_1*pos_LF[:,0,1] + mass_2*pos_LF[:,1,1])/(mass_1 + mass_2)

X1_LF = pos_LF[:,0,0]
Y1_LF = pos_LF[:,0,1]

X2_LF = pos_LF[:,1,0]
Y2_LF = pos_LF[:,1,1]

plt.plot(X1_LF-X_cdm, Y1_LF-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X2_LF-X_cdm, Y2_LF-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Adaptive timestep Leapfrog')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Leapfrog_ecc_adaptivetimestep.png')
plt.show()

particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
t = 0.
acc = 0.
h = 0.01
ts, jerk, pot= 0., 0., 0.

t0_RK = time.time()
while (t < 3*P):
    particles, ts , acc, jerk , pot = fint.integrator_Rungekutta(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_RK.append(particles.pos.copy())
    vel_RK.append(particles.vel.copy())
    h=fstep.adaptive_timestep(particles, None, None)
    t+=h
    tstep_RK.append(t)
    Etot, Ekin, Epot = integrator(particles, softening = 0.)
    deltaE= (Etot-Etot_RK[-1])/Etot 
    Etot_RK.append (Etot)
    deltaE_RK.append(deltaE)
t1_RK = time.time()

pos_RK = np.array(pos_RK)

X_cdm = (mass_1*pos_RK[:,0,0] + mass_2*pos_RK[:,1,0])/(mass_1 + mass_2)
Y_cdm = (mass_1*pos_RK[:,0,1] + mass_2*pos_RK[:,1,1])/(mass_1 + mass_2)

X1_RK = pos_RK[:,0,0]
Y1_RK = pos_RK[:,0,1]

X2_RK = pos_RK[:,1,0]
Y2_RK = pos_RK[:,1,1]

plt.plot(X1_RK-X_cdm, Y1_RK-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X2_RK-X_cdm, Y2_RK-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Adaptive timestep RungeKutta')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Rungekutta_ecc_adaptivetimestep.png')
plt.show()

particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
t = 0.
acc = 0.
h = 0.01
ts, jerk, pot= 0., 0., 0.

t0_T = time.time()
while (t < 3*P):
    particles, ts , acc, jerk , pot = fint.integrator_tsunami(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_T.append(particles.pos.copy())
    vel_T.append(particles.vel.copy())
    h=fstep.adaptive_timestep(particles, None, None)
    t+=h
    tstep_T.append(t)
    Etot, Ekin, Epot = integrator(particles, softening = 0.)
    deltaE= (Etot-Etot_T[-1])/Etot 
    Etot_T.append (Etot)
    deltaE_T.append(deltaE)
t1_T = time.time()

pos_T = np.array(pos_T)

X_cdm = (mass_1*pos_T[:,0,0] + mass_2*pos_T[:,1,0])/(mass_1 + mass_2)
Y_cdm = (mass_1*pos_T[:,0,1] + mass_2*pos_T[:,1,1])/(mass_1 + mass_2)

X1_T = pos_T[:,0,0]
Y1_T = pos_T[:,0,1]

X2_T = pos_T[:,1,0]
Y2_T = pos_T[:,1,1]

plt.plot(X1_T-X_cdm, Y1_T-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X2_T-X_cdm, Y2_T-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Adaptive timestep Tsunami')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Tsunami_ecc_adaptivetimestep.png')
plt.show()


plt.plot(tstep_E, np.abs(Etot_E), color='r', marker='o', label = 'Euler', markersize = 0.6)
plt.plot(tstep_LF, np.abs(Etot_LF), color='b', marker='o', label = 'Leapfrog', markersize = 0.6)
plt.plot(tstep_RK, np.abs(Etot_RK), color='g', marker='o', label = 'Rungekutta', markersize = 0.6)
plt.plot(tstep_T, np.abs(Etot_T), color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time')
plt.ylabel('Etot')
plt.xscale('log')
plt.title('Adaptive timestep Total energy')
plt.legend(loc='upper right', prop={'size':7})
plt.savefig('plot_Etot_ecc_adaptivetimestep.png')
plt.show()

plt.plot(tstep_E, np.abs(deltaE_E), color='r', marker='o', label = 'Euler', markersize = 0.6)
plt.plot(tstep_LF, np.abs(deltaE_LF), color='b', marker='o', label = 'Leapfrog', markersize = 0.6)
plt.plot(tstep_RK, np.abs(deltaE_RK), color='g', marker='o', label = 'Rungekutta', markersize = 0.6)
plt.plot(tstep_T, np.abs(deltaE_T), color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time')
plt.ylabel('Error Etot')
plt.xscale('log')
plt.yscale('log')
plt.title('Adaptive timestep Error energy')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Error_Etot_ecc_adaptivetimestep.png')
plt.show()

deltaE_E=np.array(deltaE_E)
deltaE_LF=np.array(deltaE_LF)
deltaE_RK=np.array(deltaE_RK)
print(np.max(deltaE_E), np.max(deltaE_LF), np.max(deltaE_RK), np.max(deltaE_T))
print(np.mean(deltaE_E), np.mean(deltaE_LF), np.mean(deltaE_RK), np.mean(deltaE_T))

print('time:', t1_E-t0_E, t1_LF-t0_LF, t1_RK- t0_RK, t1_T-t0_T)