import numpy as np
import fireworks.ic as fic
import time
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles

mass_1 = 1
mass_2 = 10
e = [0.5, 0.9, 0.99]
r_p = 1.0

for i in range (len(e)):
    ecc=e[i]
    particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
    integrator = Particles.Etot
    Etot, Ekin, Epot =integrator (particles, softening = 0.)

    a = r_p/(1.-ecc)
    Mtot = mass_1 + mass_2
    P = np.sqrt((4*np.pi**2*a**3)/(Mtot))

    pos_T = [particles.pos, ]
    vel_T = [particles.vel, ]
    Etot_T = [Etot,]
    deltaE_T=[0.,]
    t = 0.
    tstep_T = [t, ]
    h = 0.01
    acc = 0.
    ts, jerk, pot= 0., 0., 0.
    Etot, Ekin, Epot = 0., 0., 0.

    particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
    #acc = 0.
    #jerk, pot= 0., 0., 0.

    while (t < 10*P): 
        particles, efftime,_,_,_=fint.integrator_tsunami(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
        pos_T.append(particles.pos.copy())
        vel_T.append(particles.vel.copy())
        # Here we can save stuff, plot stuff, etc.
        t+=h
        tstep_T.append(t)
        Etot, Ekin, Epot = integrator(particles, softening = 0.)
        deltaE= (Etot-Etot_T[-1])/Etot 
        Etot_T.append (Etot)
        deltaE_T.append(deltaE)

    pos_T = np.array(pos_T)

    X_cdm = (mass_1*pos_T[:,0,0] + mass_2*pos_T[:,1,0])/(mass_1 + mass_2)
    Y_cdm = (mass_1*pos_T[:,0,1] + mass_2*pos_T[:,1,1])/(mass_1 + mass_2)

    X1_T = pos_T[:,0,0]
    Y1_T = pos_T[:,0,1]

    X2_T = pos_T[:,1,0]
    Y2_T = pos_T[:,1,1]

    plt.plot(X1_T-X_cdm, Y1_T-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
    plt.plot(X2_T-X_cdm, Y2_T-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Tsunami with eccentricity ' + str(ecc))
    plt.legend(loc='lower right', prop={'size':7})
    plt.savefig('plot_Tsunami_ecc '+ str(ecc) +' .png')
    plt.show()