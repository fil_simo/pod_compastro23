import numpy as np
import time
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
import fireworks.nbodylib.timesteps as fstep
from fireworks.particles import Particles

pos = np.array([[1.0, 3.0, 0.0], [-2.0, -1.0, 0.0], [1.0, -1.0, 0.0]])
vel = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
mass= np.array([3., 4., 5.])
par = Particles(pos, vel, mass)
integrator = Particles.Etot
Etot, Ekin, Epot = integrator (par, softening = 0.)

pos_RK = [par.pos, ]
vel_RK = [par.vel, ]
Etot_RK = [Etot,]
deltaE_RK = [0.,]
pos_T = [par.pos, ]
vel_T = [par.vel, ]
Etot_T = [Etot,]
deltaE_T = [0.,]
t = 0.
tevolve = 65
tstep_T = [t, ]
tstep_RK = [t, ]

acc = 0.
h = 0.1
ts, jerk, pot= 0., 0., 0.

t0_T = time.time()
while (t < tevolve):
    par, efftime,_,_,_=fint.integrator_tsunami(par, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_T.append(par.pos.copy())
    vel_T.append(par.vel.copy())
    t+=h
    tstep_T.append(t)
    Etot, Ekin, Epot = integrator(par, softening = 0.)
    #deltaE= (Etot-Etot_T[-1])/Etot 
    Etot_T.append (Etot)
    #deltaE_T.append(deltaE)
t1_T = time.time()

pos_T = np.array(pos_T)

X_cdm = (mass[0]*pos_T[:,0,0] + mass[1]*pos_T[:,1,0] + mass[2]*pos_T[:,2,0])/(mass[0] + mass[1] + mass[2])
Y_cdm = (mass[0]*pos_T[:,0,1] + mass[1]*pos_T[:,1,1] + mass[2]*pos_T[:,2,1])/(mass[0] + mass[1] + mass[2])

X1_T = pos_T[:,0,0]
Y1_T = pos_T[:,0,1]

X2_T = pos_T[:,1,0]
Y2_T = pos_T[:,1,1]

X3_T = pos_T[:,2,0]
Y3_T = pos_T[:,2,1]

plt.plot(X1_T-X_cdm, Y1_T-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X2_T-X_cdm, Y2_T-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.plot(X3_T-X_cdm, Y3_T-Y_cdm, color='g', marker='o', label = 'particle3', markersize = 0.6)
plt.xlabel('X')
plt.ylabel('Y')
plt.xlim(-7.5, 5.0)
plt.ylim(-20.0, 10.0)
plt.title('Tsunami triangle ')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Tsunami_triangle.png')
plt.show()

pos = np.array([[0.0, 0.0, 0.0], [0.5, 0.866, 0.0], [1.0, 0.0, 0.0]])
vel = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
mass= np.array([3., 4., 5.])
par = Particles(pos, vel, mass)
integrator = Particles.Etot
Etot, Ekin, Epot = integrator(par, softening = 0.)

#t = 0.
#acc = 0.
#h = 0.1
#ts, jerk, pot= 0., 0., 0.
#r_vir = -(np.sum(par.mass))**2/(4*Etot)
#epsilon = ((4*np.pi/3)*r_vir**3)**(1/3)*len(par.pos)**(-1/3)
#print(epsilon)

#t0_RK = time.time()
#while (t < tevolve): 
#    par, ts , acc, jerk , pot = fint.integrator_Rungekutta(par, h, acceleration_estimator = fdyn.acceleration_softening_vectorised, softening= epsilon, external_accelerations= None)
#    pos_RK.append(par.pos.copy())
#    vel_RK.append(par.vel.copy())
#    h=fstep.adaptive_timestep(par, None, None)
#    t+=h
#    print(t)
#    tstep_RK.append(t)
#    Etot, Ekin, Epot = integrator(par, softening = epsilon)
    #deltaE= (Etot-Etot_RK[-1])/Etot 
#    Etot_RK.append (Etot)
    #deltaE_RK.append(deltaE)
#t1_RK = time.time()

#pos_RK = np.array(pos_RK)

#X_cdm = (mass[0]*pos_RK[:,0,0] + mass[1]*pos_RK[:,1,0] + mass[2]*pos_RK[:,2,0])/(mass[0] + mass[1] + mass[2])
#Y_cdm = (mass[0]*pos_RK[:,0,1] + mass[1]*pos_RK[:,1,1] + mass[2]*pos_RK[:,2,1])/(mass[0] + mass[1] + mass[2])

#X1_RK = pos_RK[:,0,0]
#Y1_RK = pos_RK[:,0,1]

#X2_RK = pos_RK[:,1,0]
#Y2_RK = pos_RK[:,1,1]

#X3_RK = pos_RK[:,2,0]
#Y3_RK = pos_RK[:,2,1]

#plt.plot(X1_RK, Y1_RK, color='r', marker='o', label = 'particle1', markersize = 0.6)
#plt.plot(X2_RK, Y2_RK, color='b', marker='o', label = 'particle2', markersize = 0.6)
#plt.plot(X3_RK, Y3_RK, color='g', marker='o', label = 'particle3', markersize = 0.6)
#plt.xlabel('X')
#plt.ylabel('Y')
#plt.title('RungeKutta with triangle')
#plt.legend(loc='lower right', prop={'size':7})
#plt.savefig('plot_Rungekutta_triangle.png')
#plt.show()


#plt.plot(tstep_RK, Etot_RK, color='g', marker='o', label = 'RungeKutta', markersize = 0.6)
plt.plot(tstep_T, Etot_T, color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time')
plt.ylabel('Etot')
plt.title('Total energy with triangle ')
plt.legend(loc='upper left', prop={'size':7})
plt.savefig('plot_Etot_triangle.png')
plt.show()

print('time:', t1_T-t0_T)