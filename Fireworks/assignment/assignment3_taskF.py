import numpy as np
import fireworks.ic as fic
import time
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles

N = 10
mass = np.random.uniform(0, 10, N)
Q = 0.5
Epot, Ekin, Etot = 0., 0., 0.
particles = fic.ic_random_normal(N, mass)
#integrator = Particles.Etot

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
R_scale = 4*Epot*(Q-1)
v_scale = (1/2)*np.sqrt(Q/((1-Q)*Ekin))

particles.pos = particles.pos*R_scale
particles.vel = particles.vel*v_scale

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
print(Ekin, Epot, Etot)

sigma = 0.   #sigma alla seconda
Mtot = np.sum(mass)   #N*mass
sigma = -Epot/Mtot

u = 0.5 # raggio di metà massa
a = 100
r = np.sqrt(a**2/(u**(-2/3)-1))

tdyn = r/np.sqrt(sigma)
print(tdyn)

pos_T = [particles.pos, ]
vel_T = [particles.vel, ]
Etot_T = [Etot,]
t = 0.
tstep_T = [t, ]

acc = 0.
h = 0.01

t0_T = time.time()
while (t < 10*tdyn):
    particles, efftime,_,_,_=fint.integrator_tsunami(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_T.append(particles.pos.copy())
    vel_T.append(particles.vel.copy())
    t+=h
    tstep_T.append(t)
    Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
    #deltaE= (Etot-Etot_T[-1])/Etot 
    Etot_T.append (Etot)
    #deltaE_T.append(deltaE)
t1_T = time.time()

pos_T = np.array(pos_T)

X_cdm = (np.sum(mass[:]*pos_T[:,:,0]))/(Mtot)
Y_cdm = (np.sum(mass[:]*pos_T[:,:,1]))/(Mtot)

X1_T = pos_T[:,0,0]
Y1_T = pos_T[:,0,1]

X2_T = pos_T[:,1,0]
Y2_T = pos_T[:,1,1]

X3_T = pos_T[:,2,0]
Y3_T = pos_T[:,2,1]

X4_T = pos_T[:,3,0]
Y4_T = pos_T[:,3,1]

X5_T = pos_T[:,4,0]
Y5_T = pos_T[:,4,1]

X6_T = pos_T[:,5,0]
Y6_T = pos_T[:,5,1]

X7_T = pos_T[:,6,0]
Y7_T = pos_T[:,6,1]

X8_T = pos_T[:,7,0]
Y8_T = pos_T[:,7,1]

X9_T = pos_T[:,8,0]
Y9_T = pos_T[:,8,1]

X10_T = pos_T[:,9,0]
Y10_T = pos_T[:,9,1]


plt.plot(X1_T-X_cdm, Y1_T-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X2_T-X_cdm, Y2_T-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.plot(X3_T-X_cdm, Y3_T-Y_cdm, color='g', marker='o', label = 'particle3', markersize = 0.6)
plt.plot(X4_T-X_cdm, Y4_T-Y_cdm, color='gray', marker='o', label = 'particle4', markersize = 0.6)
plt.plot(X5_T-X_cdm, Y5_T-Y_cdm, color='navy', marker='o', label = 'particle5', markersize = 0.6)
plt.plot(X6_T-X_cdm, Y6_T-Y_cdm, color='purple', marker='o', label = 'particle6', markersize = 0.6)
plt.plot(X7_T-X_cdm, Y7_T-Y_cdm, color='m', marker='o', label = 'particle7', markersize = 0.6)
plt.plot(X8_T-X_cdm, Y8_T-Y_cdm, color='black', marker='o', label = 'particle8', markersize = 0.6)
plt.plot(X9_T-X_cdm, Y9_T-Y_cdm, color='aqua', marker='o', label = 'particle9', markersize = 0.6)
plt.plot(X10_T-X_cdm, Y10_T-Y_cdm, color='violet', marker='o', label = 'particle10', markersize = 0.6)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Tsunami for 10 bodies')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Tsunami_10bodies.png')
plt.show()

plt.plot(tstep_T, Etot_T, color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time')
plt.ylabel('Etot')
plt.title('Total energy for 10 bodies')
plt.legend(loc='upper right', prop={'size':7})
plt.savefig('plot_Etot_10bodies.png')
plt.show()