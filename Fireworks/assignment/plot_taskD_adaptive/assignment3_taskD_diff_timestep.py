import numpy as np
import fireworks.ic as fic
import time
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles

mass_1 = 1
mass_2 = 10
ecc = 0.5
r_p = 1.0
tstep = [0.1, 0.09, 0.07, 0.05, 0.03, 0.02, 0.01, 0.008]
Eh_E = []
Eh_RK = []

for i in range (len(tstep)):
    h = tstep[i]
    particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
    integrator = Particles.Etot
    Etot, Ekin, Epot =integrator (particles, softening = 0.)

    a = r_p/(1.-ecc)
    Mtot = mass_1 + mass_2
    P = np.sqrt((4*np.pi**2*a**3)/(Mtot))

    pos_E = [particles.pos, ]
    vel_E = [particles.vel, ]
    Etot_E = [Etot,]
    deltaE_E = [0.,]
    pos_RK = [particles.pos, ]
    vel_RK = [particles.vel, ]
    Etot_RK = [Etot,]
    deltaE_RK = [0.,]
    t = 0.
    tstep_E = [t, ]
    tstep_RK = [t, ]
    acc = 0.
    ts, jerk, pot= 0., 0., 0.
    Etot, Ekin, Epot = 0., 0., 0.

    t0_E = time.time()
    while (t < 10*P):
        particles, ts , acc, jerk , pot = fint.integrator_Euler(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
        pos_E.append(particles.pos.copy())
        vel_E.append(particles.vel.copy())
        t+=h
        tstep_E.append(t)
        Etot, Ekin, Epot = integrator(particles, softening = 0.)
        deltaE= (Etot-Etot_E[0])/Etot_E[0] 
        Etot_E.append (Etot)
        deltaE_E.append(deltaE)
    t1_E =time.time()

    deltaE_E = np.array(deltaE_E)
    Eh_E.append(np.max (deltaE_E))

    particles = fic.ic_two_body(mass_1, mass_2, r_p, ecc)
    t = 0.
    acc = 0.
    ts, jerk, pot= 0., 0., 0.

    t0_RK = time.time()
    while (t < 10*P):  #(t < 10*P)
        particles, ts , acc, jerk , pot = fint.integrator_Rungekutta(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
        pos_RK.append(particles.pos.copy())
        vel_RK.append(particles.vel.copy())
        t+=h
        tstep_RK.append(t)
        Etot, Ekin, Epot = integrator(particles, softening = 0.)
        deltaE= (Etot-Etot_RK[0])/Etot_RK[0] 
        Etot_RK.append (Etot)
        deltaE_RK.append(deltaE)
    t1_RK = time.time()

    deltaE_RK = np.array(deltaE_RK)
    Eh_RK.append(np.max (deltaE_RK))

    print('time:', t1_E-t0_E, t1_RK- t0_RK)

plt.plot(tstep, Eh_E, color='r', marker='o', label = 'Euler', markersize = 0.6)
plt.plot(tstep, Eh_RK, color='g', marker='o', label = 'Rungekutta', markersize = 0.6)
plt.xlabel('time step')
plt.ylabel('Error Etot')
plt.xscale('log')
plt.yscale('log')
plt.title('Error energy with different timestep')
plt.legend(loc='upper right', prop={'size':7})
plt.savefig('plot_Error_Etot.png')
plt.show()