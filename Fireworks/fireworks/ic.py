"""
==============================================================
Initial conditions utilities , (:mod:`fireworks.ic`)
==============================================================

This module contains functions and utilities to generate
initial conditions for the Nbody simulations.
The basic idea is that each function/class should returns
an instance of the class :class:`~fireworks.particles.Particles`

"""

import numpy as np
from .particles import Particles
import math

def ic_random_normal(N: int, mass: float=1) -> Particles:
    """
    Generate random initial condition drawing from a normal distribution
    (centred in 0 and with dispersion 1) for the position and velocity.
    The mass is instead the same for all the particles.

    :param N: number of particles to generate
    :param mass: mass of the particles
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    pos  = np.random.normal(size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    vel  = np.random.normal(size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    mass = np.ones(N)*mass

    return Particles(position=pos, velocity=vel, mass=mass)

def ic_random_uniform(N:int, mass_min:float, mass_max:float, pos_min:float, pos_max:float, vel_min:float, vel_max:float) -> Particles:

    pos = np.zeros([N,3])
    vel = np.zeros([N,3])

    mass = np.random.uniform(mass_min, mass_max, N)
    
    pos[:,0] = np.random.uniform(pos_min, pos_max, N)
    pos[:,1] = np.random.uniform(pos_min, pos_max, N)
    pos[:,2] = np.random.uniform(pos_min, pos_max, N)
    vel[:,0] = np.random.uniform(vel_min, vel_max, N)
    vel[:,1] = np.random.uniform(vel_min, vel_max, N)
    vel[:,2] = np.random.uniform(vel_min, vel_max, N)

    return Particles(mass=mass, position=pos, velocity=vel)


def ic_two_body(mass1: float, mass2: float, rp: float, e: float):
    """
    Create initial conditions for a two-body system.
    By default the two bodies will placed along the x-axis at the
    closest distance rp.
    Depending on the input eccentricity the two bodies can be in a
    circular (e=0), parabolic (e<1) or hyperbolic orbit (e>1).

    :param mass1:  mass of the first body [nbody units]
    :param mass2:  mass of the second body [nbody units]
    :param rp: closest orbital distance [nbody units]
    :param e: eccentricity
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    Mtot=mass1+mass2

    if e==1.:
        vrel=np.sqrt(2*Mtot/rp)
    else:
        a=rp/(1-e)
        vrel=np.sqrt(Mtot*(2./rp-1./a))

    # To take the component velocities
    # V1 = Vcom - m2/M Vrel
    # V2 = Vcom + m1/M Vrel
    # we assume Vcom=0.
    v1 = -mass2/Mtot * vrel
    v2 = mass1/Mtot * vrel

    pos  = np.array([[0.,0.,0.],[rp,0.,0.]])
    vel  = np.array([[0.,v1,0.],[0.,v2,0.]])
    mass = np.array([mass1,mass2])

    return Particles(position=pos, velocity=vel, mass=mass)

def kroupa_imf(x):

    N = len(x)
    m = np.zeros(N)

    a1 = 0.3
    a2 = 1.3
    a3 = 2.35
    b0 = 0.01
    b1 = 0.08
    b2 = 0.5
    b3 = 300
    A = b1**(1.-a1) - b0**(1.-a1)
    B = b2**(1.-a2) - b1**(1.-a2)
    C = b3**(1.-a3) - b2**(1.-a3)

    k2 = 1./(A*(b1**(a1-a2))/(1.-a1) + B/(1.-a2) + C*(b2**(a3-a2))/(1.-a3))
    k1 = k2*b1**(a1-a2)
    k3 = k2*b2**(a3-a2)

    I1 = k1*A/(1.-a1)
    I2 = I1 + k2*B/(1.-a2)
    
    for i in range(N):
        if (x[i] <= I1):
            m[i] = (x[i]*(1-a1)/k1 + b0**(1-a1))**(1./(1-a1))
        if ((x[i]> I1) and (x[i] <= I2)):
            m[i] = ((x[i]-I1)*(1-a2)/k2 + b1**(1-a2))**(1./(1-a2))
        if (x[i] > I2):
            m[i] = ((x[i]-I2)*(1-a3)/k3 + b2**(1-a3))**(1./(1-a3)) 

    return(m)

def Plummer_pos(x1,x2,x3,a):

    phi = x1*2.*math.pi
    theta = 1. -x2**2
    theta = np.arccos(theta)
    r = a/((x3**(-2./3.)-1.))**(-1./2.)

    x = r*np.sin(theta)*np.cos(phi)
    y = r*np.sin(theta)*np.sin(phi)
    z = r*np.cos(theta)

    xx = np.array([x,y,z])
    xx = xx.reshape((10,3))

    return xx

def gauss_r(x1,sigma):

    r = (-2.*sigma**2*np.log(1.-x1))**(1./2.)
    return r

def gauss_theta(x2):

    theta = 2.*math.pi*x2
    return theta

def coord(r,theta):

    x = r*np.cos(theta)
    y = r*np.sin(theta)
    return x,y

def maxwell(vel):

    v = (vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2])**(1./2.)
    return v

def Plummer_vel(x1,x2,sigma):

    N = len(x1)

    vel = np.zeros([3,N], float)
    for j in range(len(vel)):
        x1 = np.random.rand(N)
        x2 = np.random.rand(N)
        temp1 = gauss_r(x1,sigma)
        temp2 = gauss_theta(x2)
        vel[j], temp = coord(temp1,temp2)
    
    v = maxwell(vel)
    x1 = np.random.rand(N)
    x2 = np.random.rand(N)
    phi = x1*2.*math.pi
    theta = 1. -x2**2
    theta = np.arccos(theta)
    vx = np.zeros([N,3])

    vx[:,0] = v*np.sin(theta)*np.cos(phi)
    vx[:,1] = v*np.sin(theta)*np.sin(phi)
    vx[:,2] = v*np.cos(theta)

    return vx

def ic_cluster(m,x,y,z,a,vx,vy,sigma):

    mass = kroupa_imf(m)

    pos = Plummer_pos(x,y,z,a)

    vel = Plummer_vel(vx,vy,sigma)

    return Particles(pos, vel, mass)