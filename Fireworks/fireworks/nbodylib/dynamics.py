"""
====================================================================================================================
Collection of functions to estimate the Gravitational forces and accelerations (:mod:`fireworks.nbodylib.dynamics`)
====================================================================================================================

This module contains a collection of functions to estimate acceleration due to
gravitational  forces.

Each method implemented in this module should follow the input-output structure show for the
template function  :func:`~acceleration_estimate_template`:

Every function needs to have two input parameters:

    - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
    - softening, it is the gravitational softening. The parameters need to be included even
        if the function is not using it. Use a default value of 0.

The function needs to return a tuple containing three elements:

    - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
        this element is mandatory it cannot be 0.
    - jerk, time derivative of the acceleration, it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx3 numpy array.
    - pot, gravitational potential at the position of each particle. it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx1 numpy array.


"""
from typing import Optional, Tuple
import numpy as np
import numpy.typing as npt
from ..particles import Particles

try:
    import pyfalcon
    pyfalcon_load=True
except:
    pyfalcon_load=False

def acceleration_direct(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This an empty functions that can be used as a basic template for
    implementing the other functions to estimate the gravitational acceleration.
    Every function of this kind needs to have two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """

    mass = particles.mass
    vel = particles.vel
    pos = particles.pos
    acc  = np.zeros([len(particles),3])
    jerk = np.zeros([len(particles),3])
    pot = None
    N = len(mass)
    XvX = np.zeros([N,N])

    for i in range (N):
        for j in range (N):
            if j != i:
                dx = pos[i,0]-pos[j,0] 
                dy = pos[i,1]-pos[j,1] 
                dz = pos[i,2]-pos[j,2]

                r = np.sqrt(dx**2+dy**2+dz**2) 

                acc[i,0] -= mass[j]*dx/r**3
                acc[i,1] -= mass[j]*dy/r**3
                acc[i,2] -= mass[j]*dz/r**3

                xvx = np.dot((pos[i,:]-pos[j,:]), (vel[i,:]-vel[j,:]))
                jerk[i,:] -= mass[j]*((vel[i,:]-vel[j,:])/r**3 - 3*xvx*(pos[i,:]-pos[j,:])/r**5)

    return (acc,jerk,pot)

def acceleration_direct_vectorised(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
   
    mass = particles.mass
    vel = particles.vel
    pos = particles.pos
    acc  = np.zeros([len(particles),3])
    jerk = np.zeros([len(particles),3])
    pot = None
    N = len(mass)

    x = pos[:,0]
    y = pos[:,1]
    z = pos[:,2]
    vx = vel[:,0]
    vy = vel[:,1]
    vz = vel[:,2]

    xx = x.reshape(1,N) - x.reshape(N,1)
    yy = y.reshape(1,N) - y.reshape(N,1)
    zz = z.reshape(1,N) - z.reshape(N,1)
    vxx = vx.reshape(1,N) - vx.reshape(N,1)
    vyy = vy.reshape(1,N) - vy.reshape(N,1)
    vzz = vz.reshape(1,N) - vz.reshape(N,1)

    vv= np.multiply(xx,vxx) + np.multiply(yy,vyy) + np.multiply(zz,vzz)

    xvx = np.multiply(vv,xx)
    yvy = np.multiply(vv,yy)
    zvz = np.multiply(vv,zz)

    r = np.sqrt(np.multiply(xx,xx) + np.multiply(yy,yy) + np.multiply(zz,zz))
    r3 = r**3
    r5 = r**5

    acc[:,0] = -mass.reshape(1, N) @ np.divide(xx, r3, out=np.zeros_like(xx), where=r3!=0)
    acc[:,1] = -mass.reshape(1, N) @ np.divide(yy, r3, out=np.zeros_like(yy), where=r3!=0)
    acc[:,2] = -mass.reshape(1, N) @ np.divide(zz, r3, out=np.zeros_like(zz), where=r3!=0)

    jerk[:,0] = -mass.reshape(1,N) @ (np.divide(vxx, r3, out=np.zeros_like(vxx), where=r3!=0) - np.multiply(3,np.divide(xvx,r5, where=r5!=0)))
    jerk[:,1] = -mass.reshape(1,N) @ (np.divide(vyy, r3, out=np.zeros_like(vyy), where=r3!=0) - np.multiply(3,np.divide(yvy,r5, where=r5!=0)))
    jerk[:,2] = -mass.reshape(1,N) @ (np.divide(vzz, r3, out=np.zeros_like(vzz), where=r3!=0) - np.multiply(3,np.divide(zvz,r5, where=r5!=0)))
    
    return (acc, jerk, pot)





def acceleration_pyfalcon(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration following the fast-multipole gravity Dehnen2002 solver (https://arxiv.org/pdf/astro-ph/0202512.pdf)
    as implementd in pyfalcon (https://github.com/GalacticDynamics-Oxford/pyfalcon)

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: None, the jerk is not estimated
        - Pot: a Nx1 numpy array containing the gravitational potential at each particle position
    """

    if not pyfalcon_load: return ImportError("Pyfalcon is not available")

    acc, pot = pyfalcon.gravity(particles.pos,particles.mass,softening,kernel=0)
    jerk = None

    return acc, jerk, pot

def acceleration_softening_vectorised(particles: Particles, softening: float) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
   
    mass = particles.mass
    vel = particles.vel
    pos = particles.pos
    acc  = np.zeros([len(particles),3])
    jerk = np.zeros([len(particles),3])
    pot = None
    N = len(mass)
    ep = softening

    x = pos[:,0]
    y = pos[:,1]
    z = pos[:,2]
    vx = vel[:,0]
    vy = vel[:,1]
    vz = vel[:,2]

    xx = x.reshape(1,N) - x.reshape(N,1)
    yy = y.reshape(1,N) - y.reshape(N,1)
    zz = z.reshape(1,N) - z.reshape(N,1)
    vxx = vx.reshape(1,N) - vx.reshape(N,1)
    vyy = vy.reshape(1,N) - vy.reshape(N,1)
    vzz = vz.reshape(1,N) - vz.reshape(N,1)

    vv= np.multiply(xx,vxx) + np.multiply(yy,vyy) + np.multiply(zz,vzz)

    xvx = np.multiply(vv,xx)
    yvy = np.multiply(vv,yy)
    zvz = np.multiply(vv,zz)

    r = np.sqrt(np.multiply(xx,xx) + np.multiply(yy,yy) + np.multiply(zz,zz))
    r3 = r**3
    r5 = r**5

    S_f = r*(5.*ep**2 + 2.*r**2.)/(2.*(r**2. + ep**2)**(2.5))

    acc[:,0] = -mass.reshape(1, N) @ np.divide(np.multiply(S_f,xx), r, out=np.zeros_like(xx), where=r!=0)
    acc[:,1] = -mass.reshape(1, N) @ np.divide(np.multiply(S_f,yy), r, out=np.zeros_like(yy), where=r!=0)
    acc[:,2] = -mass.reshape(1, N) @ np.divide(np.multiply(S_f,zz), r, out=np.zeros_like(zz), where=r!=0)

    jerk[:,0] = -mass.reshape(1,N) @ (np.divide(vxx, r3, out=np.zeros_like(vxx), where=r3!=0) - np.multiply(3,np.divide(xvx,r5, where=r5!=0)))
    jerk[:,1] = -mass.reshape(1,N) @ (np.divide(vyy, r3, out=np.zeros_like(vyy), where=r3!=0) - np.multiply(3,np.divide(yvy,r5, where=r5!=0)))
    jerk[:,2] = -mass.reshape(1,N) @ (np.divide(vzz, r3, out=np.zeros_like(vzz), where=r3!=0) - np.multiply(3,np.divide(zvz,r5, where=r5!=0)))
    
    return (acc,jerk,pot)