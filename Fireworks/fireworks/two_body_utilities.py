"""
Useful functions to generate initial conditions for two-body orbits:

** Close elliptical orbits **
- from_period_to_semimajor: retrieve the semimajor-axis from the orbital period and the masses of the two bodies in a close elliptical orbit
- from_semimajor_to_period: retrieve the Period from the orbital semimajor axies and the masses of the two bodies in a close elliptical orbit
- generate_ic_binary: randomly draw the phase-space coordinates (position and velocity) from  a close two-body elliptical orbit (assuming the masses, semimajor axis and eccentricity)

** Hyperbolic orbits **
- hyperbolic_from_bv: get the orbital parameter of the hyperbolic orbit starting from the body masses, the impact parameter and velocity at iniifnity
- hyperbolic_from_rpv: get the orbital parameter of the hyperbolic orbit starting from the pericentre distance,  and velocity at infinity
- generate_ic_hyperbolic:  generate initial phase-space coordinates (position and velocity)  for an hyperbolic orbit assuming the impact parameter,
                            velocity at infinity and initial relative distance.


V1: Giuliano Iorio 29/12/23

"""

import numpy as np
from scipy import optimize


def from_period_to_semimajor(m1,m2,P):
    """
    Get the semimajor axis from the Period (in Nbody units)
    
    @param m1: mass of the first object [Nbody units]
    @param m2: mass of the second object [Nbody units]
    @param P: period [Nbody units]
    
    @return semi-major axis in Nbody units (assuming G=1)

    """
    
    Mtot = m1+m2 
    
    a = ( Mtot*P*P/(4*np.pi*np.pi) )**(1./3.)
    
    return a

def from_semimajor_to_period(m1,m2,a):
    """
    Get the Period (in Nbody units) axis from the semimajor axis (in Nbody units)
    
    @param m1: mass of the first object [Nbody units]
    @param m2: mass of the second object [Nbody units]
    @param a: semi-major axis  [Nbody units]
    
    @return period in Nbody units (assuming G=1)

    """
    
    Mtot = m1+m2 
    
    P = np.sqrt((4*np.pi*np.pi * a * a * a)/Mtot)
        
    return P

def generate_ic_binary(m1,m2,a,e,rcom=(0,0,0),vcom=(0,0,0),phi=0,inc=0):
    """
    Extract  position and velocity drawing it randomly from a  binary
    following the steps reported in Lecture-8 (slide 53).
    The stochastic part is represented by the random drawing of the mean anomaly 
    from a uniform distribution between 0 and 2pi (see Lecture 4).
    The drawn mean anomaly will set the initial positions and velocity of the two bodies
    
    NOTICE: in this case we use Nbody units with G=1,
    if you want to set a given period (in Nbody units), the correspondent semi-major axis
    can be derived using the third Kepler law and G=1,
    
    a =  (Mtot P^2 / 4 pi^2)^(1/3) (see Lecture 4)
    
    NOTICE-2: by default the binary motion will be in the x-y plane and the x axis is the axis
    connectic the ellptical foci, you can change the plane and the orientation of the 
    line connectic the foci with the input angle inc and phi
    
    @param m1: mass of the first object [Nbody units]
    @param m2: mass of the second object [Nbody units]
    @param a: semimajor-axis [Nbody units]
    @param e: eccentricity of the binary 
    @param rcom: a list of three elements containg the position of the binary centre of mass in Nbody units
    @param vcom: a list of three eleements containing the velocity of the binary centre of mass in Nbody units
    @param phi: azimuthal angle of the line containing the elliptica foci  respect to the x axis in radians 
    @param inc: inclination angle of the binary plane with respect to the x-y plane in radians 
    
    @return  a tuple containing the coordinate of the two particle + a dictionary with 
            orbital properties.
            
        Each element of the first tuple is itself a tuple containing:
    
        - x: x position of the particle in   [Nbody units]
        - y: y position of the particle in   [Nbody units]
        - z: z position of the particle in   [Nbody units]
        - vx: vx position of the particle in [Nbody units]
        - vy: vy position of the particle in [Nbody units]
        - vz: vz position of the particle in [Nbody units]
        
        The dictioanry contains the followng keys:
        - a: semi-major axis [Nbody units]
        - e: eccentricity 
        - P: period [Nbody units]
        
    """
        
    # Step1: sample the mean anomlay uniformly between 0 and 2pi
    M = np.random.uniform(0,2*np.pi)
     
    # Step2: find the eccentric anomaly solving the Kepler equation  M= E - e sin E
    ## Define the function E - esin E -M =0
    fzero = lambda E: E - e*np.sin(E) - M 
    ## Find the value of E using scipy
    sol = optimize.root_scalar(fzero, bracket=[0, 2*np.pi])
    ## get the result
    E = sol.root
    
    # Step3: find the true anomaly theta
    tt = np.sqrt((1+e)/(1-e)) * np.tan(0.5*E)
    theta = 2 * np.arctan(tt)
    
    # Step4: Find the relative separation in the Keplerian orbit 
    r = a*(1-e*e)/(1+e*np.cos(theta))
    
    # Step5: Find the Cartesian relative coordinates 
    x = r*np.cos(theta)
    y = r*np.sin(theta)
    z = 0
    
    # Step6: Find the cartesian relative velocity
    Mtot=m1+m2
    
    norm_cost = Mtot # GMtot but G=1
    
    # Find initial relative velocities (Lecture-4: Appendix A and Appendix B)
    vt = np.sqrt(norm_cost/(a*(1-e*e)))*(1+e*np.cos(theta))
    vr = np.sqrt(norm_cost/(a*(1-e*e)))*e*np.sin(theta)
    vx=vr*np.cos(theta) - vt*np.sin(theta)
    vy=vr*np.sin(theta) + vt*np.cos(theta)
    vz=0
    
    # Step7: Use Rcom and Vcom to set the final position of the particles 
    
    ## First particle position
    x1 = rcom[0] + m2/Mtot*x
    y1 = rcom[1] + m2/Mtot*y
    z1 = rcom[2]
    
    ## First particle velocity
    v1x = vcom[0] + m2/Mtot*vx
    v1y = vcom[1] + m2/Mtot*vy
    v1z = vcom[2]
    
    ## Second particle position
    x2 = rcom[0] - m1/Mtot*x
    y2 = rcom[1] - m1/Mtot*y
    z2 = rcom[2]
    
    ## Second particle velocity
    v2x = vcom[0] - m1/Mtot*vx
    v2y = vcom[1] - m1/Mtot*vy
    v2z = vcom[2]
    
    ## Now define the rotation matrix if inc not 0 or phi not 0
    if inc!=0 or phi!=0:
        # Anticlock-wise rotation around the z-axis of angle phi
        so = np.sin(phi)
        co = np.cos(phi)
        Mz = np.array([[co,so,0],[-so,co,0],[0,0,1]])
        # Anticlock-wise rotation around the y-axis
        si = np.sin(inc)
        ci = np.cos(inc)
        My = np.array([[ci,0,-si],[0,1,0],[si,0,ci]])
        # Final rotation matrix
        Rmatrix=np.dot(Mz,My)
        ## Now rotate the positions to change the plane of the binary
        x1,y1,z1=(Rmatrix@np.array([[x1,y1,z1],]).T).T[0]
        x2,y2,z2=(Rmatrix@np.array([[x2,y2,z2],]).T).T[0]

        v1x,v1y,v1z=(Rmatrix@np.array([[v1x,v1y,v1z],]).T).T[0]
        v2x,v2y,v2z=(Rmatrix@np.array([[v2x,v2y,v2z],]).T).T[0]
    
    P = np.sqrt((4*np.pi*np.pi * a * a * a)/Mtot)
    info = {"a":a, "e":e, "P":P}

    # Finaly returns the values 
    return ((x1,y1,z1,v1x,v1y,v1z),(x2,y2,z2,v2x,v2y,v2z)), info

def hyperbolic_from_bv(m1,m2,b,vinf):
    """
    Get the orbital parameter from the masses, impact parameter and v at infinity
    
    @param m1:   mass of the first object [Nbody units]
    @param m2:   mass of the second object [Nbody units]
    @param b:    impact parameter [Nbody units]
    @param vinf: velocity at infinity [Nbody units]
    @return a dictionary with the following keys:
        - a:  semimajor axis [Nbody units]
        - e:  eccentricity [Nbody units]
        - rp: pericentric distance (closest approach) [Nbody units]
        - b: impact parameter [Nbody units]
        - vinf: velocity at infinity [Nbody units]
        - T: time to periastron from the a distance that is 1000 the impact parameter [Nbody units]
    
    """
    # Get the orbital parameter
    Mtot      = m1+m2
    a         = -Mtot/(vinf*vinf)  # Semi-major axis <0, from definition of at inf Vinf = -sqrt(GM/a) (G=1) 
    e         = np.sqrt((b*b+a*a)/(a*a)) # Eccentricity e>1 
    rp        = a*(1-e) # Pericenter
    
    dist = 1000*b
    # Find the true anomaly at the input distance (Lecture-4: Appendix A)
    theta = -np.arccos( (a*(1-e*e) - dist)/(e*dist) ) 
    # NOTICE: we the the - because we want to set the beginning of the orbit in the approaching side 
    # of the orbit, the one going toward the pericentre, theta positive means that we already passed the periecentre 
 
    # Calculate the hyperbolic eccentric anomaly
    E = 2 * np.arctanh(np.sqrt((e - 1)/(e + 1)) * np.tan(theta / 2))
    # Calculate the mean anomaly
    Manomaly = e * np.sinh(E) - E
    # Time to reach the periastron from the current position
    t_to_periastron = np.abs(Manomaly)*np.sqrt(-a**3/Mtot)
    
    return {"a":a, "e":e, "rp": rp, "b":b,  "vinf": vinf, "T":t_to_periastron}

def hyperbolic_from_rpv(m1,m2,rp,vinf):
    """
    Get the orbital parameter from the masses, distance of the closest approach and v at infinity
    
    @param m1:   mass of the first object [Nbody units]
    @param m2:   mass of the second object [Nbody units]
    @param rp:   ditance of the closest approach (pericentre) [Nbody units]
    @param vinf: velocity at infinity [Nbody units]
    @return a dictionary with the following keys:
        - a:  semimajor axis [Nbody units]
        - e:  eccentricity [Nbody units]
        - rp: pericentric distance (closest approach) [Nbody units]
        - b: impact parameter [Nbody units]
        - vinf: velocity at infinity [Nbody units]
        - T: time to periastron from the a distance that is 1000 the impact parameter [Nbody units]
    
    
    """
    # Get the orbital parameter 
    Mtot      = m1+m2
    a         = -Mtot/(vinf*vinf)  # Semi-major axis <0, from definition of at inf Vinf = -sqrt(GM/a) (G=1) 
    e         = 1-rp/a
    b         = np.sqrt(a*a*e*e-a*a)
    
    
    dist = 1000*b
    # Find the true anomaly at the input distance (Lecture-4: Appendix A)
    theta = -np.arccos( (a*(1-e*e) - dist)/(e*dist) ) 
    # NOTICE: we the the - because we want to set the beginning of the orbit in the approaching side 
    # of the orbit, the one going toward the pericentre, theta positive means that we already passed the periecentre 
    # Calculate the hyperbolic eccentric anomaly
    E = 2 * np.arctanh(np.sqrt((e - 1)/(e + 1)) * np.tan(theta / 2))
    # Calculate the mean anomaly
    Manomaly = e * np.sinh(E) - E
    # Time to reach the periastron from the current position
    t_to_periastron = np.abs(Manomaly)*np.sqrt(-a**3/Mtot)
    
    return {"a":a, "e":e, "rp": rp, "b":b,  "vinf": vinf, "T":t_to_periastron}
    
    


def generate_ic_hyperbolic(m1,m2,b,vinf,dist,rcom=(0,0,0),vcom=(0,0,0),phi=0,inc=0):
    """
    This function can be used to generate the phase-space coordinates of two particles
    in a hyperbolic orbit.
    
    NOTICE: in this case we use Nbody units with G=1,
    if you want to set a given period (in Nbody units), the correspondent semi-major axis
    can be derived using the third Kepler law and G=1,
    
    NOTICE-2: by default the binary motion will be in the x-y plane and the x axis is the axis
    connectic the hyperbolic foci, you can change the plane and the orientation of the 
    line connectic the foci with the input angle inc and phi
    
    NOTICE.3; the initial conditions will be always set in the portion of the orbit that is approaching the 
                pericentre passage.
    
    @param m1: mass of the first object [Nbody units]
    @param m2: mass of the second object [Nbody units]
    @param b: impact parameter [Nbody units]
    @param vinf: velocity at infinity [Nbody units]
    @param dist: initial relative distance of the two bodies [Nbody units], the x,y, vx and vy coordinates
    are set to be self-consistent to this value
    @param rcom: a list of three elements containg the position of the binary centre of mass in Nbody units
    @param vcom: a list of three eleements containing the velocity of the binary centre of mass in Nbody units
    @param phi: azimuthal angle of the line containing the elliptica foci  respect to the x axis in radians 
    @param inc: inclination angle of the binary plane with respect to the x-y plane in radians 
    
    @return  a tuple containing the coordinate of the two particle + a dictionary with 
            orbital properties.
            
        Each element of the first tuple is itself a tuple containing:
    
        - x: x position of the particle in   [Nbody units]
        - y: y position of the particle in   [Nbody units]
        - z: z position of the particle in   [Nbody units]
        - vx: vx position of the particle in [Nbody units]
        - vy: vy position of the particle in [Nbody units]
        - vz: vz position of the particle in [Nbody units]
        
        The dictioanry contains the followng keys:
        - a: semi-major axis [Nbody units]
        - e: eccentricity 
        - rp: pericentre (closest approach) [Nbody units]
        - theta: true anomaly of the initial condition of the relative orbit
        - T: time to reach the pericentre [Nbody units]
    
    """
    
    Mtot=m1+m2
    
    # Get the orbital parameter
    a  = -Mtot/(vinf*vinf)  # Semi-major axis <0, from definition of at inf Vinf = -sqrt(GM/a) (G=1) 
    e  = np.sqrt((b*b+a*a)/(a*a)) # Eccentricity e>1 
    rp = a*(1-e) # Pericenter
    
    if dist<rp: raise ValueError(f"the input distance {dist} is smaller than the minimum radius {rp}")
    
    # Find the true anomaly at the input distance (Lecture-4: Appendix A)
    theta = -np.arccos( (a*(1-e*e) - dist)/(e*dist) ) 
    # NOTICE: we the the - because we want to set the beginning of the orbit in the approaching side 
    # of the orbit, the one going toward the pericentre, theta positive means that we already passed the periecentre 
     
    # Calculate the hyperbolic eccentric anomaly
    E = 2 * np.arctanh(np.sqrt((e - 1)/(e + 1)) * np.tan(theta / 2))
    # Calculate the mean anomaly
    Manomaly = e * np.sinh(E) - E
    # Time to reach the periastron from the current position
    t_to_periastron = np.abs(Manomaly)*np.sqrt(-a**3/Mtot)
    
    
    # Find relative x-y coordinates
    x = dist*np.cos(theta)
    y = dist*np.sin(theta)
    z = 0
    
    # Find initial relative velocities (Lecture-4: Appendix A and Appendix B)
    vmod = np.sqrt(Mtot*(2/dist - 1/a)) # Module of the velocity at input distance
    vt = np.sqrt(Mtot/(a*(1-e*e)))*(1+e*np.cos(theta)) # Tangential velocity component
    vr = np.sqrt(Mtot/(a*(1-e*e)))*e*np.sin(theta) # Radial velocity component

    vx = vr*np.cos(theta) - vt*np.sin(theta)
    vy = vr*np.sin(theta) + vt*np.cos(theta)
    vz = 0
    
    # Step7: Use Rcom and Vcom to set the final position of the particles 
    
    ## First particle position
    x1 = rcom[0] + m2/Mtot*x
    y1 = rcom[1] + m2/Mtot*y
    z1 = rcom[2]
    
    ## First particle velocity
    v1x = vcom[0] + m2/Mtot*vx
    v1y = vcom[1] + m2/Mtot*vy
    v1z = vcom[2]
    
    ## Second particle position
    x2 = rcom[0] - m1/Mtot*x
    y2 = rcom[1] - m1/Mtot*y
    z2 = rcom[2]
    
    ## Second particle velocity
    v2x = vcom[0] - m1/Mtot*vx
    v2y = vcom[1] - m1/Mtot*vy
    v2z = vcom[2]
    
    ## Now define the rotation matrix if inc not 0 or phi not 0
    if inc!=0 or phi!=0:
        # Anticlock-wise rotation around the z-axis of angle phi
        so = np.sin(phi)
        co = np.cos(phi)
        Mz = np.array([[co,so,0],[-so,co,0],[0,0,1]])
        # Anticlock-wise rotation around the y-axis
        si = np.sin(inc)
        ci = np.cos(inc)
        My = np.array([[ci,0,-si],[0,1,0],[si,0,ci]])
        # Final rotation matrix
        Rmatrix=np.dot(Mz,My)
        ## Now rotate the positions to change the plane of the binary
        x1,y1,z1=(Rmatrix@np.array([[x1,y1,z1],]).T).T[0]
        x2,y2,z2=(Rmatrix@np.array([[x2,y2,z2],]).T).T[0]

        v1x,v1y,v1z=(Rmatrix@np.array([[v1x,v1y,v1z],]).T).T[0]
        v2x,v2y,v2z=(Rmatrix@np.array([[v2x,v2y,v2z],]).T).T[0]
    
    info = {"a":a,"e":e,"rp":rp,"theta":theta,"T":t_to_periastron}
    
    return ((x1,y1,z1,v1x,v1y,v1z),(x2,y2,z2,v2x,v2y,v2z)), info


if __name__=="__main__":
    
    import matplotlib.pyplot as plt
    from fireworks.nbodylib.nunits import Nbody_units
    from fireworks.particles import Particles
    from fireworks.nbodylib.integrators import integrator_symplectic_leapfrog
    from fireworks.nbodylib.dynamics import acceleration_estimate_direct

    ## Simple example on seeting up and simulating an hyperbolic encounter 
    # Set the initial parameter 
    m1 = 10
    m2 = 3 
    b = 1 #impact parameter
    dist = 10 #initial relative distance 
    vinf = 5 # Velocity at infinity 
    

    
    # Generate initial conditions
    (p1,p2),info=generate_ic_hyperbolic(m1,m2,b,vinf,dist,rcom=(0,0,0),vcom=(0,0,0),phi=0.,inc=0.)
    
    
    # If you want to check what are the correspondent physical units, using 
    # the units M= Msun, L= 0.1 pc V= km/s and T= Myr
    nu = Nbody_units(L=0.1) # Set the scale to 0.1 pc, see documentation, https://ca23-fireworks.readthedocs.io/en/latest/api/_autosummary/fireworks.nbodylib.nunits.Nbody_units.html#fireworks.nbodylib.nunits.Nbody_units
    print(f"Initial distance in pc {nu.pos_to_pc(dist)}")
    print(f"Velocity ad infinity in km/s {nu.vel_to_kms(vinf)}")
    print(f"1 time Nbody units corresponds to {nu.t_to_Myr(1)}  Myr")
    Tperi=info["T"]
    print(f"Time needed to reach the pericentre {nu.t_to_Myr(Tperi)} Myr")
    
    # Create Particles object
    pos=np.zeros(shape=(2,3))
    pos[0] = np.array(p1[:3])
    pos[1] = np.array(p2[:3])
    vel=np.zeros_like(pos)
    vel[0] = np.array(p1[3:])
    vel[1] = np.array(p2[3:])
    mass=np.array([m1,m2])
    part = Particles(pos,vel,mass)
    
    # Simulation
    Tend = 2*info["T"] # set the simulation time to 2 times the time needed to reach the pericentre
    t=0
    hstep=0.0001
    
    pos_array=[] # array where to store the position along the orbit
    
    i=0
    while(t<Tend):
    
        part,_,_,_,_=integrator_symplectic_leapfrog(part,hstep,acceleration_estimate_direct)
        t=t+hstep
        i+=1
        
        # Save the position each 100 steps
        if i%100:
            pos_array.append(part.pos)
    pos_array=np.array(pos_array)
    
    # Plot the orbit
    plt.xlim(-5,5)
    plt.ylim(-10,10)
    plt.plot(pos_array[:,0,0],pos_array[:,0,1])
    plt.plot(pos_array[:,1,0],pos_array[:,1,1])
    plt.show()
    
    
    
    
    
    