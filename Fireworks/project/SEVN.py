import numpy as np
import fireworks.ic as fic
import matplotlib.pyplot as plt
from fireworks.nbodylib.nunits import Nbody_units
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles
from fireworks.two_body_utilities import generate_ic_binary as ic_b

from sevnpy.sevn import SEVNmanager, Star
import warnings
warnings.filterwarnings("ignore")

SEVNmanager.init()

N = 10
x = np.random.uniform(0,1,N)
y = np.random.uniform(0,1,N)
z = np.random.uniform(0,1,N)
a = 1
vx = np.random.uniform(0,1,N)
vy = np.random.uniform(0,1,N)
Q = 0.5
Epot, Ekin, Etot = 0., 0., 0.
m1,m2 = 20 , 30

m=np.random.randint(20, 300, N)
Mscale =np.sum(m)
m = m/Mscale
#print(m)
sigma = np.sqrt(1.268 * (Mscale/(1e3)) /a)

particles = fic.ic_cluster(m,x,y,z,a,vx,vy,sigma)
print("particle mass:", particles.mass)

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
R_scale = 4*Epot*(Q-1)
v_scale = (1/2)*np.sqrt(Q/((1-Q)*Ekin))

particles.pos = particles.pos*R_scale
particles.vel = particles.vel*v_scale

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
print("Energy:", Ekin, Epot, Etot)

particles.mass = particles.mass*Mscale
particles.pos= particles.pos/R_scale
particles.vel = particles.vel/v_scale
#print(particles.mass)

m1,m2 = 20, 30
a_b, e_b = 0.2, 0.
((x1,y1,z1,v1x,v1y,v1z),(x2,y2,z2,v2x,v2y,v2z)), info = ic_b(m1,m2,a_b,e_b,rcom=(0,0,0),vcom=(0,0,0),phi=0,inc=0)

pos1=np.array([x1,y1,z1])
pos2=np.array([x2,y2,z2])
pos_b=np.array([pos1,pos2])

vel1=np.array([v1x,v1y,v1z])
vel2=np.array([v2x,v2y,v2z])
vel_b=np.array([vel1,vel2])

mass_b = np.array ([m1,m2]) 

particles.mass = np.concatenate([particles.mass, mass_b])
particles.pos = np.concatenate ([particles.pos, pos_b])
particles.vel = np.concatenate([particles.vel, vel_b])

nu=Nbody_units(M=1, L=1) 
mass_physical = particles.mass
print("mass physical:", mass_physical)

stars_list = [Star(Mzams=mass_physical[0], Z=0.02,rseed=20), Star(Mzams=mass_physical[1], Z=0.02,rseed=30), 
              Star(Mzams=mass_physical[2], Z=0.02,rseed=10), Star(Mzams=mass_physical[3], Z=0.02,rseed=40),
              Star(Mzams=mass_physical[4], Z=0.02,rseed=50), Star(Mzams=mass_physical[5], Z=0.02,rseed=60),
              Star(Mzams=mass_physical[6], Z=0.02,rseed=70), Star(Mzams=mass_physical[7], Z=0.02,rseed=80),
              Star(Mzams=mass_physical[8], Z=0.02,rseed=90), Star(Mzams=mass_physical[9], Z=0.02,rseed=100),
              Star(Mzams=mass_physical[10],Z=0.02,rseed=110), Star(Mzams=mass_physical[11],Z=0.02,rseed=120)]

tlife_min = np.min([stars_list[10].tlife, stars_list[11].tlife])
Tend_nb = 1.1*nu.t_to_Nbody(tlife_min)
print("Tend:", Tend_nb)

pos_T = []
vel_T = []
mass_T = []
Etot_T = []
t = 0.
tstep_T = []
acc = 0.
h = 0.01
i = 0
mass_remnant = []
tkick = []

tcollision = []
xcollision = []
ycollision = []
ncoll=0

while (t < Tend_nb):
    particles, efftime,_,_,_=fint.integrator_tsunami(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_T.append(particles.pos.copy())
    vel_T.append(particles.vel.copy())
    t+=h
    h_physics = nu.t_to_physical(h)
    n=0
    for i in range(len(stars_list)):
        previous_type = stars_list[i].getp_array("Phase",mode="last")[0]
        stars_list[i].evolve_for(h_physics)
        updated_mass = stars_list[i].getp_array("Mass",mode="last")[0]
        #updated_radius = stars_list[i].getp_array("Radius", mode = "last")[0]
        particles.mass[i]=nu.m_to_Nbody(updated_mass)
        new_type = stars_list[i].getp_array("Phase",mode="last")[0]
        if new_type==7 and previous_type!=7:
            vkick = stars_list[i].get_SN_kick()["Vkick"]
            mass_remnant.append(particles.mass[i])
            tkick.append(t)
            if np.any(np.isnan(vkick)): 
                pass 
            else:
                particles.vel[i] = particles.vel[i] + vkick
    
    mass_T.append(particles.mass.copy())
    tstep_T.append(t)
    Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
    Etot_T.append(Etot)
            
pos_T = np.array(pos_T)
vel_T = np.array(vel_T)
mass_T = np.array(mass_T)
#print(pos_T.shape)

r=np.zeros(len(pos_T))

Mtot = np.sum(particles.mass)
X_cdm = (np.sum(particles.mass[:]*pos_T[:,:,0]))/(Mtot)
Y_cdm = (np.sum(particles.mass[:]*pos_T[:,:,1]))/(Mtot)

#coll = False

for i in range (len(pos_T)):
    for k in range (len(stars_list)):
        for j in range (k+1, len(stars_list)):
            #coll= False
            r[i] = np.sqrt((pos_T[i,k,0]-pos_T[i,j,0])**2+(pos_T[i,k,1]-pos_T[i,j,1])**2+(pos_T[i,k,2]-pos_T[i,j,2])**2)
            print("r:", r[i])
            R1 = stars_list[k].getp_array("Radius", mode ="all")    #in Rsun
            R2 = stars_list[j].getp_array("Radius", mode ="all")    #in Rsun
            #print(len(R1), len(R2))
            R_suntopc = 4.435e+7   #from Rsun to pc
            R1_pc = R1/R_suntopc    #in pc
            R2_pc = R2/R_suntopc 
            lun = np.minimum(len(R1), len(R2))
            R = R1_pc[:lun]+ R2_pc[:lun]
            if (r[i] <= R.any()):
                #if coll == True: 
                    print("collision")
                    n=n+1
                    tcollision.append(t)
                    #xcollision.append((pos_T[i,k,0]-pos_T[i,j,0])/2)
                    #ycollision.append((pos_T[i,k,1]-pos_T[i,j,1])/2)
                    xcollision.append(pos_T[i,j,0])
                    ycollision.append(pos_T[i,j,1])
                #else:
                    #print("Already collided")
               
xcollision = np.array(xcollision) 
ycollision = np.array(ycollision)
print("collisions:", n,"iter:", i)

X1_T = pos_T[:,0,0]
Y1_T = pos_T[:,0,1]

X2_T = pos_T[:,1,0]
Y2_T = pos_T[:,1,1]

X3_T = pos_T[:,2,0]
Y3_T = pos_T[:,2,1]

X4_T = pos_T[:,3,0]
Y4_T = pos_T[:,3,1]

X5_T = pos_T[:,4,0]
Y5_T = pos_T[:,4,1]

X6_T = pos_T[:,5,0]
Y6_T = pos_T[:,5,1]

X7_T = pos_T[:,6,0]
Y7_T = pos_T[:,6,1]

X8_T = pos_T[:,7,0]
Y8_T = pos_T[:,7,1]

X9_T = pos_T[:,8,0]
Y9_T = pos_T[:,8,1]

X10_T = pos_T[:,9,0]
Y10_T = pos_T[:,9,1]

X11_T = pos_T[:,10,0]
Y11_T = pos_T[:,10,1]
Z11_T = pos_T[:,10,2]

X12_T = pos_T[:,11,0]
Y12_T = pos_T[:,11,1]
Z12_T = pos_T[:,11,2]

plt.plot(tstep_T, Etot_T, color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time [Nbody]')
plt.ylabel('Etot [Nbody]')
plt.title('Total energy for 12 bodies with SEVN')
plt.legend(loc='upper right', prop={'size':7})
plt.savefig('plot_Etot_12bodies_SEVN.png')
plt.show()

plt.plot(X1_T-X_cdm, Y1_T-Y_cdm, color='olive', marker='o', label = 'particle0', markersize = 0.6)
plt.plot(X2_T-X_cdm, Y2_T-Y_cdm, color='b', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X3_T-X_cdm, Y3_T-Y_cdm, color='g', marker='o', label = 'particle2', markersize = 0.6)
plt.plot(X4_T-X_cdm, Y4_T-Y_cdm, color='gray', marker='o', label = 'particle3', markersize = 0.6)
plt.plot(X5_T-X_cdm, Y5_T-Y_cdm, color='navy', marker='o', label = 'particle4', markersize = 0.6)
plt.plot(X6_T-X_cdm, Y6_T-Y_cdm, color='purple', marker='o', label = 'particle5', markersize = 0.6)
plt.plot(X7_T-X_cdm, Y7_T-Y_cdm, color='m', marker='o', label = 'particle6', markersize = 0.6)
plt.plot(X8_T-X_cdm, Y8_T-Y_cdm, color='black', marker='o', label = 'particle7', markersize = 0.6)
plt.plot(X9_T-X_cdm, Y9_T-Y_cdm, color='aqua', marker='o', label = 'particle8', markersize = 0.6)
plt.plot(X10_T-X_cdm, Y10_T-Y_cdm, color='violet', marker='o', label = 'particle9', markersize = 0.6)
plt.plot(X11_T-X_cdm, Y11_T-Y_cdm, color='gold', marker='o', label = 'particle10', markersize = 0.6)
plt.plot(X12_T-X_cdm, Y12_T-Y_cdm, color='chocolate', marker='o', label = 'particle11', markersize = 0.6)
plt.scatter(xcollision-X_cdm, ycollision-Y_cdm, c = 'r', marker = 'X', label = 'collision', s = 50.)
plt.xlabel('X [Nbody]')
plt.ylabel('Y [Nbody]')
plt.title('Tsunami for 12 bodies with SEVN')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Tsunami_12bodies_SEVN.png')
plt.show()

plt.plot(tstep_T, mass_T.T[0, :], color='olive', marker='o', label = 'particle0', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[1, :], color='b', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[2, :], color='g', marker='o', label = 'particle2', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[3, :], color='gray', marker='o', label = 'particle3', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[4, :], color='navy', marker='o', label = 'particle4', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[5, :], color='purple', marker='o', label = 'particle5', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[6, :], color='m', marker='o', label = 'particle6', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[7, :], color='black', marker='o', label = 'particle7', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[8, :], color='aqua', marker='o', label = 'particle8', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[9, :], color='violet', marker='o', label = 'particle9', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[10, :], color='gold', marker='o', label = 'particle10', markersize = 0.6)
plt.plot(tstep_T, mass_T.T[11, :], color='chocolate', marker='o', label = 'particle11', markersize = 0.6)
plt.scatter(tkick, mass_remnant, c = 'r', marker = 'X', label = 'mass kick', s = 50.)
plt.xlabel('time [Nbody]')
plt.ylabel('Mass [Nbody]')
plt.title('Mass for 12 bodies with SEVN')
plt.legend(loc='upper right', prop={'size':7})
plt.savefig('plot_mass_12bodies_SEVN.png')
plt.show()

#import matplotlib.animation as animation
#from IPython import display 
#import mpl_toolkits.mplot3d.axes3d as p3

#fig = plt.figure()
#axis = fig.add_subplot(projection='3d')

# Setting the axes properties
#axis.set(xlim3d = (np.min(X11_T)-1, np.max(X11_T)+1), xlabel='X')
#axis.set(ylim3d = (np.min(Y11_T)-1, np.max(Y11_T)+1), ylabel='Y')
#axis.set(zlim3d = (np.min(Z11_T)-1, np.max(Z11_T)+1), zlabel='Z')

#per il titolo  
#fig.suptitle('Animation binary 3D SEVN', fontsize=14)

#lines = [axis.plot([], [], [])[0] for _ in range(12)]

#def update(frame):
#    for i in range(12):
#        xdata = pos_T[:frame//20,i,0]
#        ydata = pos_T[:frame//20,i,1]
#        zdata = pos_T[:frame//20,i,2]
#        lines[i].set_data(xdata, ydata)
#        lines[i].set_3d_properties(zdata)
#    return lines

#ani = animation.FuncAnimation(fig, update, frames=len(X11_T)*20,  interval=10, blit=True)
#ani.save('binary3d_SEVN.gif', writer='Pillow', fps = 60)
#plt.show()

SEVNmanager.close()
