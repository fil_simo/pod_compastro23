import numpy as np
import fireworks.ic as fic
import matplotlib.pyplot as plt
from fireworks.nbodylib.nunits import Nbody_units
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles
from fireworks.two_body_utilities import generate_ic_binary as ic_b

from sevnpy.sevn import SEVNmanager, Star
import warnings
warnings.filterwarnings("ignore")

SEVNmanager.init()

mass1, mass2 = 30, 20 # In Msun 
a_b, e_b = 0.2, 0.
((x1,y1,z1,v1x,v1y,v1z),(x2,y2,z2,v2x,v2y,v2z)), info = ic_b(mass1,mass2,a_b,e_b,rcom=(0,0,0),vcom=(0,0,0),phi=0,inc=0)

pos1=np.array([x1,y1,z1])
pos2=np.array([x2,y2,z2])
pos_b=np.array([pos1,pos2])
#print(pos_b)

vel1=np.array([v1x,v1y,v1z])
vel2=np.array([v2x,v2y,v2z])
vel_b=np.array([vel1,vel2])
#print(vel_b)

mass_b = np.array ([mass1,mass2])
R_sun_pc = 2.25e-8
R_sun = 695e3
nu=Nbody_units(M=1, L=1) 
mass1_nb = nu.m_to_Nbody(mass1)
mass2_nb = nu.m_to_Nbody(mass2)

particles=Particles(position=np.array([pos1,pos2]),velocity=np.array([vel1,vel2]),mass=np.array([mass1_nb,mass2_nb]))

stars_list = [Star(Mzams=mass1,Z=0.02,rseed=110), Star(Mzams=mass2,Z=0.02,rseed=120)]

tlife_min = np.min([stars_list[0].tlife, stars_list[1].tlife])
Tend_nb = 1.1*nu.t_to_Nbody(tlife_min)
print(Tend_nb)

pos_T = []
vel_T = []
mass_T = []
Etot_T = []
t = 0.
tstep_T = []
acc = 0.
h = 0.001
i = 0
tcollision = []
xcollision = []
ycollision = []

while (t < Tend_nb):
    particles, efftime,_,_,_=fint.integrator_tsunami(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_T.append(particles.pos.copy())
    vel_T.append(particles.vel.copy())
    t+=h
    h_physics = nu.t_to_physical(h)
    for i in range(len(stars_list)):
        previous_type = stars_list[i].getp_array("Phase",mode="last")[0]
        stars_list[i].evolve_for(h_physics)
        updated_mass = stars_list[i].getp_array("Mass",mode="last")[0]
        particles.mass[i]=nu.m_to_Nbody(updated_mass)
        new_type = stars_list[i].getp_array("Phase",mode="last")[0]
        if new_type==7 and previous_type!=7:
            vkick = stars_list[i].get_SN_kick()["Vkick"]
            print(vkick)
            print('BH')
            if np.any(np.isnan(vkick)): 
                pass 
            else:
                particles.vel[i] = particles.vel[i] + vkick
        for j in range (i+1, len(stars_list)):
            r = np.linalg.norm(particles.pos[i,:]-particles.pos[j,:])
            R1 = stars_list[i].getp_array("Radius",mode="last")[0]
            R2 = stars_list[j].getp_array("Radius",mode="last")[0]
            R1_nb = R1/R_sun
            #nu.pos_to_Nbody(R1)
            R2_nb = R2/R_sun
            #nu.pos_to_Nbody(R2)
            if (r <= R1_nb + R2_nb):
                print("collision")
                tcollision.append(t)
                xcollision.append((particles.pos[i,0]-particles.pos[j,0])/2)
                ycollision.append((particles.pos[i,1]-particles.pos[j,1])/2)
    
    mass_T.append(particles.mass.copy())
    tstep_T.append(t)

    Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
    Etot_T.append(Etot)

pos_T = np.array(pos_T)
vel_T = np.array(vel_T)
mass_T = np.array(mass_T)

xcollision = np.array(xcollision)
ycollision = np.array(ycollision)

Mtot = np.sum(particles.mass)
print(Mtot)

X_cdm = (np.sum(particles.mass[:]*pos_T[:,:,0]))/(Mtot)
Y_cdm = (np.sum(particles.mass[:]*pos_T[:,:,1]))/(Mtot)

X1_T = pos_T[:,0,0]
Y1_T = pos_T[:,0,1]

X2_T = pos_T[:,1,0]
Y2_T = pos_T[:,1,1]

plt.plot(tstep_T, Etot_T, color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time')
plt.ylabel('Etot')
plt.title('Total energy for binary with SEVN')
plt.legend(loc='upper right', prop={'size':7})
plt.savefig('plot_Etot_binary_SEVN.png')
plt.show()

plt.plot(X1_T-X_cdm, Y1_T-Y_cdm, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X2_T-X_cdm, Y2_T-Y_cdm, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.scatter(xcollision -X_cdm, ycollision -Y_cdm, c = 'r', marker = 'X', label = 'collision', s = 50.)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Tsunami for binary with SEVN')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Tsunami_binary_SEVN.png')
plt.show()

NN = len(pos_T)

fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(15, 12))
plt.subplots_adjust(hspace=0.5)
Epot, Ekin, Etot = 0., 0., 0.
J = 0.
Etot_B = []
e_B = []
a_B = []
P_B = []

mass1 = particles.mass[0]
mass2 = particles.mass[1]
mass = np.array([mass1,mass2])

for j in range(NN):

    pos1 = pos_T[j,0,:]
    pos2 = pos_T[j,1,:]
    pos = np.array([pos1,pos2])

    vel1 = vel_T[j,0,:]
    vel2 = vel_T[j,1,:]
    vel = np.array([vel1,vel2])

    par = Particles(pos, vel, mass)

    poscdm = Particles.com_pos(par)
    velcdm = Particles.com_vel(par)

    par.pos = par.pos - poscdm
    par.vel = par.vel - velcdm

    parr = Particles(par.pos, par.vel, mass)

    Etot, Ekin, Epot = Particles.Etot(parr, softening = 0.)
    Etot_B.append(Etot)
    J = Particles.Ang_mom(parr, softening= 0.)

    M = parr.mass[0]+parr.mass[1]
    a = -parr.mass[0]*parr.mass[1]/(2*Etot)
    if (a >= 0):
        P = np.sqrt(4*np.pi**2*a**3/M)
    if (a < 0):
        P = -1

    a_B.append(a)
    P_B.append(P)

    mu = parr.mass[0]*parr.mass[1]/(parr.mass[0]+parr.mass[1])
    e2 = 1+(2*Etot*J**2)/((mu**3)*(parr.mass[1]+parr.mass[0])**2)
    e=np.sqrt(e2)
    if (e>1):
        e = 2
    e_B.append(e)

X1_B = pos_T[:,0,0]
Y1_B = pos_T[:,0,1]
Z1_B = pos_T[:,0,2]

X2_B = pos_T[:,1,0]
Y2_B = pos_T[:,1,1]
Z2_B = pos_T[:,1,2]

r = np.sqrt((X1_B - X2_B)**2 + (Y1_B - Y2_B)**2 + (Z1_B - Z2_B)**2)

axs[0][0].plot(X1_B, Y1_B, label= 'Particle 10')
axs[0][0].plot(X2_B, Y2_B, label= 'Particle 11')
axs[0][0].set_title('Particles trajectory')
axs[0][0].set_xlabel('X')
axs[0][0].set_ylabel('Y')
axs[0][0].legend()

axs[0][1].plot(tstep_T, r, label='Relative distance', color= 'violet')
axs[0][1].set_title('Relative distance')
axs[0][1].set_xlabel('t')
axs[0][1].set_ylabel('r')

axs[1][0].plot(tstep_T, Etot_B, label='Total energy', color='c')
axs[1][0].set_title('Total energy')
axs[1][0].set_xlabel('t')
axs[1][0].set_ylabel('Etot')

axs[1][1].plot(tstep_T, a_B, label='Semi-major axis', color='b')
axs[1][1].set_title('Semi-major axis')
axs[1][1].set_xlabel('t')
axs[1][1].set_ylabel('a')

axs[2][0].plot(tstep_T, P_B, label='Orbital period', color= 'g')
axs[2][0].set_title('Orbital period')
axs[2][0].set_xlabel('t')
axs[2][0].set_ylabel('P')

axs[2][1].plot(tstep_T, e_B, label='Eccentricity',color= 'r')
axs[2][1].set_title('Eccentricity')
axs[2][1].set_xlabel('t')
axs[2][1].set_ylabel('e')

plt.savefig('binary_SEVN.png')
plt.show()

SEVNmanager.close()
