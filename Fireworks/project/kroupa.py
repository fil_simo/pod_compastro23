import numpy as np
import matplotlib.pyplot as plt
import fireworks.ic as fic


N = 10**6
x = np.random.uniform(0,1,N)
m = fic.kroupa_imf(x)

a=np.log10(np.min(m))
b=np.log10(np.max(m))
mybins=np.logspace(a,b,num=50)
plt.hist(m,bins=mybins, density='True',log='True')
plt.xscale("log")
plt.savefig('plot.png')
plt.show()

