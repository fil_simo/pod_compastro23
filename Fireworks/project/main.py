import numpy as np
import fireworks.ic as fic
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles

N = 10
m = np.random.uniform(0,1,N)
x = np.random.uniform(0,1,N)
y = np.random.uniform(0,1,N)
z = np.random.uniform(0,1,N)
a = 10
vx = np.random.uniform(0,1,N)
vy = np.random.uniform(0,1,N)
Q = 0.5
Epot, Ekin, Etot = 0., 0., 0.

mass = fic.kroupa_imf(m)
Mtot = np.sum(mass)
sigma = np.sqrt(1.268 * (Mtot/(1e3)) /a)
print(sigma)

particles = fic.ic_cluster(m, x, y, z, a,vx,vy,sigma)

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
R_scale = 4*Epot*(Q-1)
v_scale = (1/2)*np.sqrt(Q/((1-Q)*Ekin))

particles.pos = particles.pos*R_scale
particles.vel = particles.vel*v_scale

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
print(Ekin, Epot, Etot)

u = 0.5 # raggio di metà massa
a = 10
r = np.sqrt(a**2/(u**(-2/3)-1))

tdyn = r/np.sqrt(sigma)
print(tdyn)

pos_T = []
vel_T = []
Etot_T = []
t = 0.
tstep_T = []
acc = 0.
h = 0.01
iid1 = []
iid2 = []
i = 0

while (t < 10*tdyn):
    particles, efftime,_,_,_=fint.integrator_tsunami(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_T.append(particles.pos.copy())
    vel_T.append(particles.vel.copy())
    t+=h
    tstep_T.append(t)
    Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
    Etot_T.append(Etot)
    ID = Particles.binary(particles, softening = 0.)
    for i in range(len(ID)):
        if (ID[i,0] not in iid1) or (ID[i,1] not in iid2):
            iid1.append(ID[i,0]) 
            iid2.append(ID[i,1])


iid = np.array([iid1,iid2]).T
print(iid)

pos_T = np.array(pos_T)
vel_T = np.array(vel_T)

X_cdm = (np.sum(particles.mass[:]*pos_T[:,:,0]))/(Mtot)
Y_cdm = (np.sum(particles.mass[:]*pos_T[:,:,1]))/(Mtot)

X1_T = pos_T[:,0,0]
Y1_T = pos_T[:,0,1]

X2_T = pos_T[:,1,0]
Y2_T = pos_T[:,1,1]

X3_T = pos_T[:,2,0]
Y3_T = pos_T[:,2,1]

X4_T = pos_T[:,3,0]
Y4_T = pos_T[:,3,1]

X5_T = pos_T[:,4,0]
Y5_T = pos_T[:,4,1]

X6_T = pos_T[:,5,0]
Y6_T = pos_T[:,5,1]

X7_T = pos_T[:,6,0]
Y7_T = pos_T[:,6,1]

X8_T = pos_T[:,7,0]
Y8_T = pos_T[:,7,1]

X9_T = pos_T[:,8,0]
Y9_T = pos_T[:,8,1]

X10_T = pos_T[:,9,0]
Y10_T = pos_T[:,9,1]

plt.plot(tstep_T, Etot_T, color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time [Nbody]')
plt.ylabel('Etot [Nbody]')
plt.title('Total energy for 10 bodies ')
plt.legend(loc='upper right', prop={'size':7})
plt.savefig('plot_Etot_10bodies.png')
plt.show()

plt.plot(X1_T-X_cdm, Y1_T-Y_cdm, color='r', marker='o', label = 'particle0', markersize = 0.6)
plt.plot(X2_T-X_cdm, Y2_T-Y_cdm, color='b', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(X3_T-X_cdm, Y3_T-Y_cdm, color='g', marker='o', label = 'particle2', markersize = 0.6)
plt.plot(X4_T-X_cdm, Y4_T-Y_cdm, color='gray', marker='o', label = 'particle3', markersize = 0.6)
plt.plot(X5_T-X_cdm, Y5_T-Y_cdm, color='navy', marker='o', label = 'particle4', markersize = 0.6)
plt.plot(X6_T-X_cdm, Y6_T-Y_cdm, color='purple', marker='o', label = 'particle5', markersize = 0.6)
plt.plot(X7_T-X_cdm, Y7_T-Y_cdm, color='m', marker='o', label = 'particle6', markersize = 0.6)
plt.plot(X8_T-X_cdm, Y8_T-Y_cdm, color='black', marker='o', label = 'particle7', markersize = 0.6)
plt.plot(X9_T-X_cdm, Y9_T-Y_cdm, color='aqua', marker='o', label = 'particle8', markersize = 0.6)
plt.plot(X10_T-X_cdm, Y10_T-Y_cdm, color='violet', marker='o', label = 'particle9', markersize = 0.6)
plt.xlabel('X [Nbody]')
plt.ylabel('Y [Nbody]')
plt.xlim([-100,100])
plt.ylim([-100,100])
plt.title('Tsunami for 10 bodies')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_Tsunami_10bodies.png')
plt.show()

N = len(iid)
NN = len(pos_T)

for i in range(N):
    fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(15, 12))
    plt.subplots_adjust(hspace=0.5)
    Epot, Ekin, Etot = 0., 0., 0.
    J = 0.
    Etot_B = []
    e_B = []
    a_B = []
    P_B = []

    mass1 = particles.mass[iid[i,0]]
    mass2 = particles.mass[iid[i,1]]
    mass = np.array([mass1,mass2])

    for j in range(NN):

        pos1 = pos_T[j,iid[i,0],:]
        pos2 = pos_T[j,iid[i,1],:]
        pos = np.array([pos1,pos2])

        vel1 = vel_T[j,iid[i,0],:]
        vel2 = vel_T[j,iid[i,1],:]
        vel = np.array([vel1,vel2])

        par = Particles(pos, vel, mass)

        poscdm = Particles.com_pos(par)
        velcdm = Particles.com_vel(par)

        par.pos = par.pos - poscdm
        par.vel = par.vel - velcdm

        parr = Particles(par.pos, par.vel, mass)

        Etot, Ekin, Epot = Particles.Etot(parr, softening = 0.)
        Etot_B.append(Etot)
        J = Particles.Ang_mom(parr, softening= 0.)

        M = parr.mass[0]+parr.mass[1]
        a = -parr.mass[0]*parr.mass[1]/(2*Etot)
        if (a >= 0):
            P = np.sqrt(4*np.pi**2*a**3/M)
        if (a < 0):
            P = -1

        a_B.append(a)
        P_B.append(P)

        mu = parr.mass[0]*parr.mass[1]/(parr.mass[0]+parr.mass[1])
        e2 = 1+(2*Etot*J**2)/((mu**3)*(parr.mass[1]+parr.mass[0])**2)
        e=np.sqrt(e2)
        if (e>1):
            e = 2
        e_B.append(e)

    X1_B = pos_T[:,iid[i,0],0]
    Y1_B = pos_T[:,iid[i,0],1]
    Z1_B = pos_T[:,iid[i,0],2]

    X2_B = pos_T[:,iid[i,1],0]
    Y2_B = pos_T[:,iid[i,1],1]
    Z2_B = pos_T[:,iid[i,1],2]

    Etot_B =np.array(Etot_B)
    Etot_T =np.array(Etot_T)

    r = np.sqrt((X1_B - X2_B)**2 + (Y1_B - Y2_B)**2 + (Z1_B - Z2_B)**2)

    axs[0][0].plot(X1_B, Y1_B, label= 'Particle'+ str(iid[i][0]))
    axs[0][0].plot(X2_B, Y2_B, label= 'Particle'+ str(iid[i][1]))
    axs[0][0].set_title('Particles trajectory')
    axs[0][0].set_xlabel('X [Nbody]')
    axs[0][0].set_ylabel('Y [Nbody]')
    axs[0][0].legend()

    axs[0][1].plot(tstep_T, r, label='Relative distance', color= 'violet')
    axs[0][1].set_title('Relative distance')
    axs[0][1].set_xlabel('t [Nbody]')
    axs[0][1].set_ylabel('r [Nbody]')

    deltaE = np.abs(Etot_B-Etot_T)/Etot_B
    #axs[1][0].plot(tstep_T, Etot_B, label='Total energy', color='c')
    axs[1][0].plot(tstep_T, deltaE, color='c')
    axs[1][0].set_title('Relative energy between binary and cluster')
    axs[1][0].set_xlabel('t [Nbody]')
    axs[1][0].set_ylabel('$\Delta$ E/E_B')

    axs[1][1].plot(tstep_T, a_B, label='Semi-major axis', color='b')
    axs[1][1].set_title('Semi-major axis')
    axs[1][1].set_xlabel('t [Nbody]')
    axs[1][1].set_ylabel('a [Nbody]')
    #axs[1][1].set_ylim ([-50,50])

    axs[2][0].plot(tstep_T, P_B, label='Orbital period', color= 'g')
    axs[2][0].set_title('Orbital period')
    axs[2][0].set_xlabel('t [Nbody]')
    axs[2][0].set_ylabel('P [Nbody]')
    #axs[2][0].set_ylim([-100,100])

    axs[2][1].plot(tstep_T, e_B, label='Eccentricity', color= 'r')
    axs[2][1].set_title('Eccentricity')
    axs[2][1].set_xlabel('t [Nbody]')
    axs[2][1].set_ylabel('e')
    #axs[2][1].set_ylim([0,2])

    plt.savefig('binary_evolution' + str(i)+ '.png')
    plt.show()
    
