import numpy as np
import fireworks.ic as fic
import matplotlib.pyplot as plt
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles
from fireworks.two_body_utilities import generate_ic_binary as ic_b

N = 10
m = np.random.uniform(0,1,N)
x = np.random.uniform(0,1,N)
y = np.random.uniform(0,1,N)
z = np.random.uniform(0,1,N)
a = 10
vx = np.random.uniform(0,1,N)
vy = np.random.uniform(0,1,N)
Q = 0.5
Epot, Ekin, Etot = 0., 0., 0.

mass = fic.kroupa_imf(m)
Mtot = np.sum(mass)
sigma = np.sqrt(1.268 * (Mtot/(1e3)) /a)
print(mass)
particles = fic.ic_cluster(m, x, y, z, a,vx,vy,sigma)

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
R_scale = 4*Epot*(Q-1)
v_scale = (1/2)*np.sqrt(Q/((1-Q)*Ekin))

particles.pos = particles.pos*R_scale
particles.vel = particles.vel*v_scale

Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
print(Ekin, Epot, Etot)


m1,m2 = 1.,2.
a_b, e_b = 0.2, 0.
((x1,y1,z1,v1x,v1y,v1z),(x2,y2,z2,v2x,v2y,v2z)), info = ic_b(m1,m2,a_b,e_b,rcom=(0,0,0),vcom=(0,0,0),phi=0,inc=0)

pos1=np.array([x1,y1,z1])
pos2=np.array([x2,y2,z2])
pos_b=np.array([pos1,pos2])

vel1=np.array([v1x,v1y,v1z])
vel2=np.array([v2x,v2y,v2z])
vel_b=np.array([vel1,vel2])

mass_b = np.array ([m1,m2]) 

particles.mass = np.concatenate([m,mass_b])
particles.pos = np.concatenate ([particles.pos, pos_b])
particles.vel = np.concatenate([particles.vel, vel_b])

u = 0.5 # raggio di metà massa
a = 10
r = np.sqrt(a**2/(u**(-2/3)-1))

tdyn = r/np.sqrt(sigma)
print(tdyn)

pos_T = [ ]
vel_T = [ ]
Etot_T = []
t = 0.
tstep_T = [ ]
acc = 0.
h = 0.01
iid1 = []
iid2 = []
i = 0

while (t < 5*tdyn):
    particles, efftime,_,_,_=fint.integrator_tsunami(particles, h, acceleration_estimator = fdyn.acceleration_pyfalcon, softening= 0., external_accelerations= None)
    pos_T.append(particles.pos.copy())
    vel_T.append(particles.vel.copy())
    t+=h
    tstep_T.append(t)
    Etot, Ekin, Epot = Particles.Etot(particles, softening = 0.)
    Etot_T.append(Etot)

pos_T = np.array(pos_T)
vel_T = np.array(vel_T)

X_cdm = (np.sum(particles.mass[:]*pos_T[:,:,0]))/(Mtot)
Y_cdm = (np.sum(particles.mass[:]*pos_T[:,:,1]))/(Mtot)

V1_T =[]
V2_T =[]
V3_T =[]
V4_T =[]
V5_T =[]
V6_T =[]
V7_T =[]
V8_T =[]
V9_T =[]
V10_T =[]
V11_T =[]
V12_T =[]
for i in range (len(vel_T)):
    V1_T.append(np.linalg.norm(vel_T[i,0,:]))
    V2_T.append(np.linalg.norm(vel_T[i,1,:]))
    V3_T.append(np.linalg.norm(vel_T[i,2,:]))
    V4_T.append(np.linalg.norm(vel_T[i,3,:]))
    V5_T.append(np.linalg.norm(vel_T[i,4,:]))
    V6_T.append(np.linalg.norm(vel_T[i,5,:]))
    V7_T.append(np.linalg.norm(vel_T[i,6,:]))
    V8_T.append(np.linalg.norm(vel_T[i,7,:]))
    V9_T.append(np.linalg.norm(vel_T[i,8,:]))
    V10_T.append(np.linalg.norm(vel_T[i,9,:]))
    V11_T.append(np.linalg.norm(vel_T[i,10,:]))
    V12_T.append(np.linalg.norm(vel_T[i,11,:]))

X1_T = pos_T[:,0,0]
Y1_T = pos_T[:,0,1]

X2_T = pos_T[:,1,0]
Y2_T = pos_T[:,1,1]

X3_T = pos_T[:,2,0]
Y3_T = pos_T[:,2,1]

X4_T = pos_T[:,3,0]
Y4_T = pos_T[:,3,1]

X5_T = pos_T[:,4,0]
Y5_T = pos_T[:,4,1]

X6_T = pos_T[:,5,0]
Y6_T = pos_T[:,5,1]

X7_T = pos_T[:,6,0]
Y7_T = pos_T[:,6,1]

X8_T = pos_T[:,7,0]
Y8_T = pos_T[:,7,1]

X9_T = pos_T[:,8,0]
Y9_T = pos_T[:,8,1]

X10_T = pos_T[:,9,0]
Y10_T = pos_T[:,9,1]

X11_T = pos_T[:,10,0]
Y11_T = pos_T[:,10,1]

X12_T = pos_T[:,11,0]
Y12_T = pos_T[:,11,1]

plt.plot(tstep_T, Etot_T, color='y', marker='o', label = 'Tsunami', markersize = 0.6)
plt.xlabel('time')
plt.ylabel('Etot')
plt.title('Total energy for 12 bodies ')
plt.legend(loc='upper right', prop={'size':7})
#plt.savefig('plot_Etot_12bodies.png')
plt.show()

plt.plot(tstep_T, V1_T, color='r', marker='o', label = 'particle1', markersize = 0.6)
plt.plot(tstep_T, V2_T, color='b', marker='o', label = 'particle2', markersize = 0.6)
plt.plot(tstep_T, V3_T, color='g', marker='o', label = 'particle3', markersize = 0.6)
plt.plot(tstep_T, V4_T, color='gray', marker='o', label = 'particle4', markersize = 0.6)
plt.plot(tstep_T, V5_T, color='navy', marker='o', label = 'particle5', markersize = 0.6)
plt.plot(tstep_T, V6_T, color='purple', marker='o', label = 'particle6', markersize = 0.6)
plt.plot(tstep_T, V7_T, color='m', marker='o', label = 'particle7', markersize = 0.6)
plt.plot(tstep_T, V8_T, color='black', marker='o', label = 'particle8', markersize = 0.6)
plt.plot(tstep_T, V9_T, color='aqua', marker='o', label = 'particle9', markersize = 0.6)
plt.plot(tstep_T, V10_T, color='violet', marker='o', label = 'particle10', markersize = 0.6)
plt.plot(tstep_T, V11_T, color='gold', marker='o', label = 'particle11', markersize = 0.6)
plt.plot(tstep_T, V12_T, color='chocolate', marker='o', label = 'particle12', markersize = 0.6)
plt.xlabel('t')
plt.ylabel('vel')
plt.title('vel for 12 bodies')
plt.legend(loc='lower right', prop={'size':7})
plt.savefig('plot_vel_12bodies.png')
plt.show()

NN = len(pos_T)

fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(15, 12))
plt.subplots_adjust(hspace=0.5)
Epot, Ekin, Etot = 0., 0., 0.
J = 0.
Etot_B = []
e_B = []
a_B = []
P_B = []
J_B = []

mass1 = particles.mass[10]
mass2 = particles.mass[11]
mass = np.array([mass1,mass2])

for j in range(NN):

    pos1 = pos_T[j,10,:]
    pos2 = pos_T[j,11,:]
    pos = np.array([pos1,pos2])

    vel1 = vel_T[j,10,:]
    vel2 = vel_T[j,11,:]
    vel = np.array([vel1,vel2])

    par = Particles(pos, vel, mass)

    poscdm = Particles.com_pos(par)
    velcdm = Particles.com_vel(par)

    par.pos = par.pos - poscdm
    par.vel = par.vel - velcdm

    parr = Particles(par.pos, par.vel, mass)

    Etot, Ekin, Epot = Particles.Etot(parr, softening = 0.)
    Etot_B.append(Etot)
    J = Particles.Ang_mom(parr, softening= 0.)
    J_B.append(J)

    M = parr.mass[0]+parr.mass[1]
    a = -parr.mass[0]*parr.mass[1]/(2*Etot)
    if (a >= 0):
        P = np.sqrt(4*np.pi**2*a**3/M)
    if (a < 0):
        P = -1

    a_B.append(a)
    P_B.append(P)

    mu = parr.mass[0]*parr.mass[1]/(parr.mass[0]+parr.mass[1])
    e2 = 1+(2*Etot*J**2)/((mu**3)*(parr.mass[1]+parr.mass[0])**2)
    e=np.sqrt(e2)
    if (e>1):
        e = 2
    e_B.append(e)

X1_B = pos_T[:,10,0]
Y1_B = pos_T[:,10,1]
Z1_B = pos_T[:,10,2]

X2_B = pos_T[:,11,0]
Y2_B = pos_T[:,11,1]
Z2_B = pos_T[:,11,2]

r = np.sqrt((X1_B - X2_B)**2 + (Y1_B - Y2_B)**2 + (Z1_B - Z2_B)**2)

axs[0][0].plot(X1_B, Y1_B, label= 'Particle 10')
axs[0][0].plot(X2_B, Y2_B, label= 'Particle 11')
axs[0][0].set_title('Particles trajectory')
axs[0][0].set_xlabel('X')
axs[0][0].set_ylabel('Y')
axs[0][0].legend()

axs[0][1].plot(tstep_T, r, label='Relative distance', color= 'violet')
axs[0][1].set_title('Relative distance')
axs[0][1].set_xlabel('t')
axs[0][1].set_ylabel('r')

#deltaE = np.abs(Etot_B-Etot_T)/Etot_B
axs[1][0].plot(tstep_T, Etot_B, label='Total energy', color='c')
#axs[1][0].plot(tstep_T, deltaE, label='Total energy', color='c')
axs[1][0].set_title('Total energy')
axs[1][0].set_xlabel('t')
axs[1][0].set_ylabel('Etot')

axs[1][1].plot(tstep_T, a_B, label='Semi-major axis', color='b')
axs[1][1].set_title('Semi-major axis')
axs[1][1].set_xlabel('t')
axs[1][1].set_ylabel('a')

axs[2][0].plot(tstep_T, P_B, label='Orbital period', color= 'g')
axs[2][0].set_title('Orbital period')
axs[2][0].set_xlabel('t')
axs[2][0].set_ylabel('P')

axs[2][1].plot(tstep_T, e_B, label='Eccentricity',color= 'r')
axs[2][1].set_title('Eccentricity')
axs[2][1].set_xlabel('t')
axs[2][1].set_ylabel('e')

#plt.savefig('binary_evolution.png')
plt.show()
    
