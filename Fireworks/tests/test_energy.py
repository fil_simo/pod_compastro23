import pytest
import numpy as np
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
from fireworks.ic import ic_random_normal

def test_energy():
    """
    Apply the simple_test the integrator_template

    """
    integrator= Particles.Etot

    pos = np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]])
    vel = np.array([[0.0, 0.0, 0.0], [-1.0, 0.0, 0.0]])
    mass= np.ones(len(pos))
    par_old = Particles(pos, vel, mass)

    ekin_fake = 0.5
    epot_fake = -1.0
    etot_fake = ekin_fake + epot_fake
    Etot, Ekin, Epot = integrator(par_old, softening = 0.)

    # Test velocity, we expect no change in velocity (we use a very small number instead of 0)
    # in order to take into account possible round-off errors
    assert np.all( np.abs(ekin_fake -Ekin) <= 1e-10)
    assert np.all( np.abs(epot_fake -Epot) <= 1e-10)
    assert np.all( np.abs(etot_fake -Etot) <= 1e-10)
