[33mcommit 7584602d848db9c961fbb1d8ae25cc3f48d5a604[m[33m ([m[1;36mHEAD -> [m[1;32mmain[m[33m, [m[1;31morigin/assignment1_taskC[m[33m, [m[1;32massignment1_taskC[m[33m)[m
Author: ylenia mascolo <ylenia.mascolo@studenti.unipd.it>
Date:   Fri Oct 27 08:11:14 2023 +0000

    Delete taskC_ym

[33mcommit b3c3140907cf2cfaca70e22a33899fded49f8f80[m
Author: ylenia mascolo <ylenia.mascolo@studenti.unipd.it>
Date:   Fri Oct 27 08:09:46 2023 +0000

    Add new file

[33mcommit 75ba24bf5c7c2a883e53e214b28a31faa38ecd0c[m
Author: Filippo Simonato <filippo.simonato.1@studenti.unipd.it>
Date:   Fri Oct 27 08:03:58 2023 +0000

    Add new file

[33mcommit 55775269140f0d5cd4126271c7e0bc32629359f6[m
Author: ylenia mascolo <ylenia.mascolo@studenti.unipd.it>
Date:   Fri Oct 27 08:02:45 2023 +0000

    Upload New File

[33mcommit 9f8a9ae8412a2396fad06a2fc03df193fafff07c[m
Author: ilaria giovannini <ilaria.giovannini@studenti.unipd.it>
Date:   Fri Oct 27 08:01:55 2023 +0000

    Upload New File

[33mcommit 7372bbc07a0b2a64350e322a1dcde20b46720a79[m[33m ([m[1;31morigin/main[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: ilaria giovannini <ilaria.giovannini@studenti.unipd.it>
Date:   Fri Oct 27 07:35:11 2023 +0000

    Update assignment1_taskB.txt

[33mcommit 0a506434a23aef9c368688b80a8a6908222f06fb[m
Author: ylenia mascolo <ylenia.mascolo@studenti.unipd.it>
Date:   Fri Oct 27 07:33:51 2023 +0000

    Add new file

[33mcommit 752776ad382b1e947e82e00a021aaf4756618b68[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Thu Oct 26 09:23:26 2023 +0200

    A bit of cleaning

[33mcommit 3296295cdbf41fa79caa8fbaf9d5267ee5e6b657[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Tue Oct 24 00:33:23 2023 +0200

    Added the proper Henon scaling

[33mcommit 41fbf3811105ef15fda59e7187519b7174ce02d2[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Tue Oct 24 00:16:15 2023 +0200

    correct typo in documentation

[33mcommit 9b9a59212836bbb5714b7902aabcb0012f2c1e11[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Mon Oct 23 15:25:59 2023 +0200

    removed trash files

[33mcommit ef820a2782cbf9e3d9168d92283b2e6038d05629[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Mon Oct 23 15:22:23 2023 +0200

    added numpy import in nunits module

[33mcommit a0e0399d2b0005f10350a343908ef708dd4f09e9[m
Merge: af3d0a8 811bb6b
Author: Giuliano <giuliano.iorio89@gmail.com>
Date:   Tue Oct 17 12:09:03 2023 +0000

    Merge branch 'newbranch' into 'main'
    
    Newbranch
    
    See merge request iogiul/pod_compastro23!3

[33mcommit 811bb6be9e1e12dea193e4b4518ac00ac7be701a[m
Author: iogiul <giuliano.iorio.astro@gmail.com>
Date:   Tue Oct 17 12:06:42 2023 +0000

    2 new file in new branch

[33mcommit 9739a2c74b3739344aa754000abd158aa27a75ed[m
Author: iogiul <giuliano.iorio.astro@gmail.com>
Date:   Tue Oct 17 11:33:33 2023 +0000

    new file created

[33mcommit 1c265b42ca9548f9d8f35ad084862bb9274f072f[m
Author: iogiul <giuliano.iorio.astro@gmail.com>
Date:   Tue Oct 17 11:19:40 2023 +0000

    removed newfile2 and pther stuff

[33mcommit d8fbc114a64d99c0830bc32dd078f743f2e3d8c1[m
Author: iogiul <giuliano.iorio.astro@gmail.com>
Date:   Tue Oct 17 11:18:16 2023 +0000

    removed newfile2 and pther stuff

[33mcommit 9d0a5b497d6fa7a89a691ccf8a7be23f3ed6ed4d[m
Author: iogiul <giuliano.iorio.astro@gmail.com>
Date:   Tue Oct 17 11:11:09 2023 +0000

    Created two new files

[33mcommit af3d0a8d6cf4d387396f060e68e12a4a93379efe[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Mon Oct 16 23:20:16 2023 +0200

    Added logo

[33mcommit 7e0deb0f2975d92668c3af014fa2f81e194fd558[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sun Oct 15 12:56:45 2023 +0200

    Added the potential Point_Mass and the related tests

[33mcommit 671bc6825528f3a956c50c3fd9514bab0b8d9225[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sun Oct 15 12:14:21 2023 +0200

    now particles radius returns a Nx1 array

[33mcommit 631ffef4f7fed566ee452caf7414ebadb119d254[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sun Oct 15 12:11:07 2023 +0200

    now particles radius returns a Nx1 array

[33mcommit 1ca1acede5565e5b0a06d019726b95e61261ae97[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sun Oct 15 11:47:03 2023 +0200

    documentation updated

[33mcommit 7e5b32fdc7cc84920d1a1ec8d73457aa212deaab[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sun Oct 15 11:41:08 2023 +0200

    Added a check if pyfalcon is not loaded. Added the parameter external_acceleration on the integrator template

[33mcommit 1e3b67ac4042451cf2a5f44bef02c87c00248607[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 23:38:33 2023 +0200

    added ic_two_body

[33mcommit 127a309c2e9d3a7a84b7a0a437271691ef3ed33e[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 23:34:30 2023 +0200

    Added the ic generator function ic_two_body and the related tests

[33mcommit e89a880923824df237071722138ca2f8bacb842f[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 17:39:55 2023 +0200

    Added a test on the integrators

[33mcommit f803a4f0706002747d7b88b66fe1fb3ae5ea303e[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 17:34:25 2023 +0200

    fixed a typo in particles

[33mcommit 78f8138b66cec9b61d9da4ad04e74e057f4edf6a[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 17:32:07 2023 +0200

    fixed an array type for input of tsunami in the tsunami wrapper

[33mcommit f6553055927ed8adeb18be6a2952d97c0b2461e3[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 17:28:48 2023 +0200

    force the pos, vel and mass array type to be float in Particles

[33mcommit aed52e270ac001392a2e5766ef0db09417cdb498[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 17:24:45 2023 +0200

    fixed a typo on the tsunami integrator wrapper

[33mcommit 72e61fa3b9b37da1be51253624685d5ee97fc035[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 16:56:37 2023 +0200

    Documnetation updated

[33mcommit ea08c47c1bc5e2e7557722ee0003121af933c97b[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 13:45:31 2023 +0200

    test_ic cleaned

[33mcommit 3dde9cbf434a5b2ea10e49b4bde664395f30c694[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 13:11:07 2023 +0200

    added test file for ic

[33mcommit 2c9b19212a6c70e765c7e129d91f59530fa88777[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Sat Oct 14 12:56:19 2023 +0200

    created the submodule ic.py and added the function ic_random_normal

[33mcommit 1c89e46e79a1c81d6548bbfee6aa143b5006e028[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Fri Oct 13 00:12:20 2023 +0200

    updated doc conf.py file adding a mock import of pyfalcon

[33mcommit 4653bafd9ed9bcabc4c4bef0da046829b785f35e[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Fri Oct 13 00:08:10 2023 +0200

    Fixed a string in .readthedocs.yaml

[33mcommit 3df3a831b17e629c724494c59f9ad2abc211d5f6[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Fri Oct 13 00:06:37 2023 +0200

    Added requirements for building documentation in readthedocs

[33mcommit 6d5b971ea2f9f4b8be84b2cc886b3d7e6348baf2[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Fri Oct 13 00:00:54 2023 +0200

    Added readthedocs file

[33mcommit a8f14810ac7338ebe46141f149389b82212f480d[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Thu Oct 12 17:49:14 2023 +0200

    Added two tests for the exerices 1

[33mcommit d96d53520722912512f15cf09a18eaaf7e89aa65[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Thu Oct 12 17:10:25 2023 +0200

    First working version of the fireworks modules (still to be tested, thouhg)

[33mcommit cbce5d5f0ec84183068f9d8499d7fdcea0823abf[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Tue Oct 3 09:52:39 2023 +0200

    Added twine in the Docker file

[33mcommit 24fa64db07763200e77bb883f7627b3f3b0deadf[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Tue Oct 3 09:25:37 2023 +0200

    Added pytest in the Dockerfile

[33mcommit e52f51db3a791491becc7dcecd440c9d2713e2ef[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Thu Sep 28 17:14:15 2023 +0200

    First commit (first fireworks version + Docker images)

[33mcommit e0a05deea0ce492a7af93213ed5aeeb443192f00[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Thu Sep 28 17:03:04 2023 +0200

    Added the class Particles, and the folder with the sphinx documentation

[33mcommit f2f5638ea96662215c7ce85dcb7fe951bd2678c5[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Wed Sep 27 17:30:40 2023 +0200

    First commit (first fireworks version + Docker images)

[33mcommit 291492e25838d9e13429d654fc93f57565364766[m
Author: giulianoiorio <giuliano.iorio89@gmail.com>
Date:   Wed Sep 27 11:52:29 2023 +0200

    First commit

[33mcommit 1081dc0600d2791aafe43648408d6f69f3edcefe[m
Author: Giuliano <giuliano.iorio89@gmail.com>
Date:   Thu Aug 24 11:31:12 2023 +0000

    Update README.md

[33mcommit 45eb1dca159c09fcd9d657c66caad783d69e81e3[m
Author: Giuliano <giuliano.iorio89@gmail.com>
Date:   Thu Aug 24 11:30:32 2023 +0000

    Update README.md

[33mcommit b314dd1d82793534712888bc70119ef69c2cd622[m
Author: Giuliano <giuliano.iorio89@gmail.com>
Date:   Thu Aug 24 11:29:13 2023 +0000

    Initial commit
